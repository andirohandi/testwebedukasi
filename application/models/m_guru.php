<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_guru extends CI_Model {

	private $table = "tbl_guru";
	private $id = "ID";

	// Create
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	function getSelect($where='', $limit='', $offset='') {
		$this->db->select('a.*');
		
		$this->db->select('b.JABATAN as NAMA_JABATAN, a.STATUS as STATUS_GURU, a.ID AS ID_GURU',FALSE);
		$this->db->join('tbl_jabatan b', 'a.JABATAN = b.ID', 'left');

		
		
		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get('tbl_guru a');
		else                                     
			$query = $this->db->get('tbl_guru a', $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	// Update
	public function getUpdate($dt,$id)
	{
		$this->db->set($dt);
		$this->db->where('ID',$id);
		$this->db->update($this->table);
	}
	
	
	function getDetail($where=''){
		$data = array();
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_guru');
		
		$data = $query->row_array();
		$query->free_result();
		
		return $data;
	}
	
	function count($where='') {
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
		$query->free_result();
	}
	
	function get($where='') {
		$data = array();
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_guru tbl');
		
		$data = $query->row_array();
		$query->free_result();
		
		return $data;
	}
	
	function getProfile($id='') {
		$query = $this->db->where('ID',$id)
			->get($this->table);
		
		$data = $query->row_array();
		
		return $data;
	}
}