<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_front extends CI_Model {

	private $table = "tbl_setting";
	private $id = "ID";

	// Create
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	function getSelect($where='', $limit='', $offset='', $kategori='') {
		
		$query = $this->db->get($this->table);
		
		return $query;
		$query->free_result();
	}
	
	function getUpdate($dt) {
		$this->db->set($dt)
			->update($this->table);
	}
	
}