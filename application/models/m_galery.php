<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_post extends CI_Model {

	private $table = "tbl_post";
	private $id = "ID";

	// Create
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
}