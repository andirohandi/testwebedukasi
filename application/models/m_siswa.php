<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_siswa extends CI_Model {

	private $table = "tbl_siswa";
	private $id = "ID";

	// Create
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}

	function getSelect($where='', $limit='', $offset='') {

		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get($this->table);
		else                                     
			$query = $this->db->get($this->table, $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	// Update
	public function getUpdate($dt,$id)
	{
		$this->db->set($dt);
		$this->db->where('ID',$id);
		$this->db->update($this->table);
	}
	
	function count($where='') {
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
		$query->free_result();
	}
	
	function getDetail($where=''){
		$data = array();
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_siswa');
		
		$data = $query->row_array();
		$query->free_result();
		
		return $data;
	}
	
	function get($where='') {
		$data = array();
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_siswa tbl');
		
		$data = $query->row_array();
		$query->free_result();
		
		return $data;
	}
	
	function getSelectPencapaian($id) {
		return $this->db->where('ID_SISWA',$id)
			->order_by('TAHUN','ASC')
			->get('tbl_pencapaian');
		
	} 
	
	function cekPencapaian($id_siswa,$tahun) {
		return $this->db->where('TAHUN',$tahun)
			->where('ID_SISWA',$id_siswa)
			->get('tbl_pencapaian');
	}
	
	public function getInsertPencapaian($dt)
	{
		$this->db->set($dt);
		$this->db->insert('tbl_pencapaian');
	}
	
	// getKomentar
	function getHistoryComent($id='') {
		$this->db->select('a.*, b.*, c.*');
		$this->db->select('c.NAMA as NAMA_PENGIRIM, a.ID_FORUM AS ID_F',FALSE);
		$this->db->join('tbl_forum b','a.ID_FORUM = b.ID','left');
		$this->db->join('tbl_guru c','b.PENGIRIM = c.ID','left');
		
		$query = $this->db->where('a.NO_INDUK',$id)
					->order_by('a.ID','DESC')
					->get('tbl_komentar a');
		$data = $query->result();
		$query->free_result();
		
		return $data;
	}
	
	function getProfile($id='') {
		$query = $this->db->where('ID',$id)
			->get($this->table);
		
		$data = $query->row_array();
		
		return $data;
	}
	
}