<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_forum extends CI_Model {

	private $table = "tbl_forum";
	private $id = "ID";

	// Create
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	// Update
	public function getUpdate($dt,$id)
	{
		$this->db->set($dt);
		$this->db->where('ID',$id);
		$this->db->update($this->table);
	}
	
	function getSelect($where='', $limit='', $offset='') {
		$this->db->select('a.*');
		
		$this->db->select('a.ID AS ID_FORUM, b.NIP as NIP, b.NAMA AS NAMA, a.STATUS AS STATUS_FORUM, b.IMAGE as IMAGE, b.EMAIL as EMAIL',FALSE);
		$this->db->join('tbl_guru b', 'a.PENGIRIM = b.ID', 'left');
		$this->db->order_by('a.ID','DESC');
		
		
		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get('tbl_forum a');
		else                                     
			$query = $this->db->get('tbl_forum a', $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	function getDetail($id=''){
		$data = array();
		
		$this->db->select('a.*')
			->select('a.ID AS ID, a.STATUS as STATUS, b.NAMA as NAMA',FALSE)
			->join('tbl_guru b','a.PENGIRIM=b.ID','left');
		
		if($id != '')
			$this->db->where("a.ID",$id);
		
		$query = $this->db->get('tbl_forum a');
		
		$data = $query->row_array();
		$query->free_result();
		
		return $data;
	}
	
	function count($where='') {
		
		$this->db->select('a.*');
		
		$this->db->select('a.ID AS ID_FORUM',FALSE);
		$this->db->join('tbl_guru b', 'a.PENGIRIM = b.ID', 'left');
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get('tbl_forum a');
		
		return $query->num_rows();
		$query->free_result();
	}
	
	// hitung komentar
	function countComent($id='') {
		$this->db->where('ID_FORUM',$id);
		return $this->db->get('tbl_komentar')->num_rows();
	}
	
	function getComent($id) {
		return $this->db->where('ID_FORUM',$id)
			->get('tbl_komentar');
	}
}