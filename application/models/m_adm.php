<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_adm extends CI_Model {

	private $table = "tbl_user";
	private $id = "ID";
	
	public function cekLogin($data)
	{
		$this->db->where($data);
		$query = $this->db->get($this->table);
		
		if($query->num_rows()==1)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}
	
	public function cekLoginGuru($nip)
	{
		$this->db->where('NIP',$nip);
		$this->db->where('STATUS',1);
		$query = $this->db->get('tbl_guru');
		
		if($query->num_rows()==1)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}
	
	public function cekLoginSiswa($nis)
	{
		$this->db->where('NIS',$nis);
		$this->db->where('STATUS',1);
		$query = $this->db->get('tbl_siswa');
		
		if($query->num_rows()==1)
		{
			return $query->result();
		}else
		{
			return false;
		}
	}

}