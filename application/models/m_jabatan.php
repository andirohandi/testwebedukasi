<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_jabatan extends CI_Model {

	private $table = "tbl_jabatan";
	private $id = "ID";
	
	
	public function getInsert($dt)
	{
		$this->db->set($dt);
		$this->db->insert($this->table);
	}
	
	function getSelect($where='', $limit='', $offset='') {
		
		if($where)
			$this->db->where($where);
		
		if(!$limit && !$offset)
			$query = $this->db->get($this->table);
		else                                     
			$query = $this->db->get($this->table, $limit, $offset);
		
		return $query;
		$query->free_result();
	}
	
	//membuat select option
	function getSelectOption($where=''){
		if($where) $this->db->where($where);
		
		$query = $this->db->get($this->table);
		$result = $query->result();
		return $result;
	}
	
	function count($where='') {
		
		if($where)
			$this->db->where($where);
		
		$query = $this->db->get($this->table);
		
		return $query->num_rows();
		$query->free_result();
	}
	
}