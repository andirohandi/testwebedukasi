<div id="msg" class='col-md-12'>
			<?php echo decode($msg)?>
</div>
<div class="row">
	<div class='col-md-12 col-sm-12'>
		<div>
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home Image</a></li>
				<li role="presentation"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">About</a></li>
				<li role="presentation"><a href="#visi" aria-controls="visi" role="tab" data-toggle="tab">Visi Misi</a></li>
			</ul>
		  
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="home"><?php $this->load->view('front/vwHome');?></div>
				<div role="tabpanel" class="tab-pane fade" id="about"><?php $this->load->view('front/vwAbout');?></div>
				<div role="tabpanel" class="tab-pane fade" id="visi"><?php $this->load->view('front/vwVisi');?></div>
			</div>
		</div>
	</div>
</div>