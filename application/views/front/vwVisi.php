<br/>
<div class='row'>
	<div class='col-md-12 col-sm-12'>
		<?php 
			foreach($list->result() as $row) {
				echo $row->VISI_MISI;
			}
		?>
		
	</div>
	<div class="col-md-12 col-sm-12">
		<br/><br/>
		<div class="form-group">
			<label>Ubah Visi Misi</label>
			<textarea name="vm" id="vm" class="form-control" style='height:250px'></textarea>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<button type="button" name='' id='' onclick='ubahVisiMisi()' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan</button>
				<a href='<?php echo site_url('guru')?>' type="button" name='kembali' id='kembali' class='btn btn-default pull-right' style='margin-right:10px'><i class='fa fa-remove'></i> Kembali</a>
			</div>
		</div>
	</div>
</div>

<script>
function ubahVisiMisi() {
		var visi = tinyMCE.get('vm').getContent();
		
		$.ajax({
			url  : 'front/updateVisi',
			data : {visi:visi},
			type : 'POST',
			dataType : 'JSON',
			success : function(rs) {
				if(rs.rs==1) {
					alertify.success('Visi Misi  berhasil diubah');
					tinyMCE.get('vm').setContent('');
					location.reload();
				}else{
					alertify.alert('Visi Misi  gagal diubah');
				}
			}
		});
	}
</script>