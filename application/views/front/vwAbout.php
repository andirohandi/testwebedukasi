<script src="<?php echo base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>
$(document).ready(function(){
	tinymce.init({selector: 'textarea',
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		relative_urls: false
	});
	
	
});
function ubahAbout() {
		var about = tinyMCE.get('ab').getContent();
		
		$.ajax({
			url  : 'front/updateAbout',
			data : {about:about},
			type : 'POST',
			dataType : 'JSON',
			success : function(rs) {
				if(rs.rs==1) {
					alertify.success('Tentang sekolah berhasil diubah');
					tinyMCE.get('ab').setContent('');
					location.reload();
				}else{
					alertify.alert('Tentang sekolah gagal diubah');
				}
			}
		});
	}
</script>
<br/>
<div class='row'>
	<div class='col-md-12 col-sm-12'>
		<?php 
			foreach($list->result() as $row) {
				echo $row->ABOUT;
			}
		?>
		
	</div>
	
	<div class="col-md-12 col-sm-12">
		<br/><br/>
		<div class="form-group">
			<label>Ubah Tentang Sekolah</label>
			<textarea name="ab" id="ab" class="form-control" style='height:250px'></textarea>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<button type="button" name='simpan' id='simpan' onclick='ubahAbout()' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan</button>
				<a href='<?php echo site_url('guru')?>' type="button" name='kembali' id='kembali' class='btn btn-default pull-right' style='margin-right:10px'><i class='fa fa-remove'></i> Kembali</a>
			</div>
		</div>
	</div>
</div>