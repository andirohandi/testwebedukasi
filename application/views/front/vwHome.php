<style>
.form-group.success, .form-group.success input, .form-group.success select, .form-group.success textarea{color:#468847;border-color:#468847}
.form-group.error, .form-group.error input, .form-group.error select, .form-group.error textarea{color:#b94a48;border-color:#b94a48}
.drop-area{
  width:100px;
  height:45px;
  border: 1px solid #999;
  text-align: center;
  padding:10px;
  cursor:pointer;
}
#thumbnail img{
  width:155px;
  height:155px;
  margin:5px;
}
canvas{
  border:1px solid red;
}

</style>
<script>
$(document).ready(function(){
	
	var fileDiv = document.getElementById("upload");
		var fileInput = document.getElementById("upload-image");
		
		console.log(fileInput);
		
		fileInput.addEventListener("change",function(e){
		  var files = this.files
		  showThumbnail(files)
		},false)

		fileDiv.addEventListener("click",function(e){
		  $(fileInput).show().focus().click().hide();
		  e.preventDefault();
		},false)

		fileDiv.addEventListener("dragenter",function(e){
		  e.stopPropagation();
		  e.preventDefault();
		},false);

		fileDiv.addEventListener("dragover",function(e){
		  e.stopPropagation();
		  e.preventDefault();
		},false);

		fileDiv.addEventListener("drop",function(e){
		  e.stopPropagation();
		  e.preventDefault();

		  var dt = e.dataTransfer;
		  var files = dt.files;

		  showThumbnail(files)
		},false);
		
	});

function showThumbnail(files){
	  for(var i=0;i<files.length;i++){
		var file = files[i]
		var imageType = /image.*/

		if(!file.type.match(imageType)){
		  console.log("Not an Image");
		  continue;
		}

		var image = document.createElement("img");
		// image.classList.add("")
		var thumbnail = document.getElementById("thumbnail");
		image.file = file;
		
		while(thumbnail.hasChildNodes()) {
			thumbnail.removeChild(thumbnail.lastChild);
		}
		
		thumbnail.appendChild(image)
		
		$('#addImage').hide();
		
		var reader = new FileReader()
		reader.onload = (function(aImg){
		  return function(e){
			aImg.src = e.target.result;
		  };
		}(image))
		var ret = reader.readAsDataURL(file);
		var canvas = document.createElement("canvas");
		ctx = canvas.getContext("2d");
		image.onload= function(){
		  ctx.drawImage(image,100,100)
		}
	  }
	}
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})

	
</script>

<br/>
<div class='row'>
	<div class='col-md-8 col-sm-12'>
		<?php 
			foreach($list->result() as $row) {
				?>
					<img src='<?php echo base_url($row->IMAGE_HOME)?>' class='thumbnail img-responsive'/>
				<?php
			}
		?>
		
	</div>
	<div class="col-md-4 col-sm-12">
		<form action='<?php echo site_url('front/updateHome')?>' method='POST' enctype='multipart/form-data'>
			<div class="form-group">
				<div class="row">
					<label class="col-md-12 col-sm-12">Ubah Gambar</label>
				</div>
				<input type="file" style="display:none" class="form-control" id="upload-image" name="upload-image" multiple="multiple"></input>
				
				<div id="upload" class="btn btn-default" >				
					<i id='addImage' class='glyphicon glyphicon-plus' style='width:90px'> Add</i>
					<div id="thumbnail"></div>
				</div>
				<button type="submit" name='simpan' id='simpan' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan </button>
			</div>
		</form>
	</div>
</div>