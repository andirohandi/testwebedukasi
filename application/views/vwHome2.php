<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title;?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- Bootstrap style --> 
		<link href="<?php echo HTTP_MT_FRONT;?>/css/materialize.min.css" rel="stylesheet" media="screen,projection"/>
		<link href="<?php base_url()?>assets/front/logo.png" rel="shortcut icon" />
		
		<link href="<?php echo HTTP_MT_FRONT;?>/css/icon.css" rel="stylesheet" media="screen"/>
		<script src="<?php echo HTTP_MT_FRONT?>/js/jquery-2.1.1.min.js" type="text/javascript"></script>
		<script src="<?php echo HTTP_MT_FRONT?>/js/materialize.min.js" type="text/javascript"></script>
	</head>
	<body   class=''>
		<?php $this->load->view('layout/vwHeader2')?>
		<div class='container'>
			<div class='row'>
				<?php $this->load->view($view)?>
				
				
				<?php $this->load->view('layout/vwSidebar2') ?>
			</div>
			<div class='row'>
				<div class='card-panel  blue hoverable col s12 m12 l12' style='height:60px'>
					<p><b>LATEST GALLERY</b></p>
				</div>
					
			</div>
			<div class='row z-depth-1' style='margin-top:-30px'>
					<?php
						$like 	= '';
						$limit	= 4;
						$offset = '';
						$kategori = 3;
						$lis = $this->m_post->getSelect($like, $limit, $offset, $kategori);
						
						foreach($lis->result() as $r) {
							?>
								<div class='card-panel col s12 m3 l3 z-depth-0'>
									<div class='' >
										<img class="materialboxed responsive-img hoverable" data-caption="<?php echo $r->JUDUL?>" src="<?php echo base_url($r->IMAGE)?>" >
										<center><?php echo $r->JUDUL?></center>
									</div>
								</div>
							<?php
						}
					?>
			</div>
		</div>
		<div class='row' style='margin-bottom:-100px;padding-bottom:0px'>
			<div class='card-panel  blue accent-4 hoverable col s12 m12 l12'>
				<p style='text-align:center;color:grey;padding:10px'>Copyright @ 2015 <a href='https://www.facebook.com/secangkirairdigurunpasir'>Andi Rohandi</a></p>
			</div>
		</div>
		
		<script>
			$(document).ready(function(){
				$('.materialboxed').materialbox();
			  });

			$(function() {
				var float_nav_offset_top = $('#explain').offset().top;
				var float_nav = function(){ 
					var scroll_top = $(window).scrollTop();
					
					if (scroll_top > float_nav_offset_top) { 
						$('#explain').css({ 'position': 'fixed', 'top':0, 'left':0, 'margin':'auto' });
						
					} else {
						$('#explain').css({ 'position': 'relative', 'margin-top':'-20px'}); 
					}
				};
				$(window).scroll(function() { 
				float_nav(); 
				});
			});

		</script>
    </body>
</html>