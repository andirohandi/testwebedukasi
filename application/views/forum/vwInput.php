
<script src="<?php echo base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
	
</script>

<?php $id = (isset($forum['ID']));?>
<div class="row">
	<form action='<?php echo site_url('forum/create')?>' method='POST' enctype='multipart/form-data'>
		<div id="msg" class='col-md-12'>
			<?php echo decode($msg);?>
		</div>	
		<div class="col-md-3 col-sm-12">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<label>Nama Pengirim</label>
						<input type="text" class="form-control" id="nama" name="nama" value="<?php echo $id !='' ? $forum['NAMA'] : $this->session->userdata('username') ?>" readonly >
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<label>Status Forum</label>
						<select id="status" name="status" class='form-control' required>
							<option value='1' <?php echo $id  ? ($forum['STATUS']==1 ?'selected':'') : ''?> >AKTIF</option>
							<option value='0' <?php echo $id ? ($forum['STATUS']==0 ?'selected':'') : '' ?>>TIDAK AKTIF</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-12">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<label>Tanggal Forum</label>
						<input type="date" class="form-control" id="tanggal_post" name="tanggal_post" value='<?php echo $id ? tgl_indo($forum['TGL_INPUT']) : tgl_indo(date('Y-m-d'))?>' readonly >
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="form-group">
				<input type="hidden" name="id_forum" id="id_forum" class="form-control" value='<?php echo (isset($forum['ID']) ? encode($forum['ID']) : encode('')) ?>'>
				<label>Judul</label>
				<input type="text" class="form-control" id="judul" name="judul" value="<?php echo $id ? $forum['JUDUL'] : ''?>" placeholder='Masukkan judul forum . .' required>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="form-group">
				<label>Deskripsi Forum</label>
				<textarea name="deskripsi" id="deskripsi" class="form-control" style='height:250px'><?php echo (isset($forum['ID']) ? $forum['DESKRIPSI'] : '') ?></textarea>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<button type="submit" name='simpan' id='simpan' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan Forum</button>
					<a href='<?php echo site_url('forum')?>' type="button" name='kembali' id='kembali' class='btn btn-default pull-right' style='margin-right:10px'><i class='fa fa-remove'></i> Kembali</a>
				</div>
			</div>
		</div>
	</form>
</div>
<br/>
<br/>
<br/>
