<style>
.error{
	color: #FB3A3A;
    padding: 0;
    text-align: left;
}
#kanan-menu{
	float:right;

}

</style>
<div class="row">
	<div class="col-md-1 col-xs-12" style="margin-top:5px;">
		<?php if($this->session->userdata('level')==2) { ?>
		<span data-toggle="tooltip" data-placement='top' title='Tambah Post'><a href="<?php echo site_url('post/input')?>" class="btn btn-success btn-sm btn-circle" ><i class="fa fa-plus"></i></a></span>
		<?php } ?>
	</div>
	
	<div class="col-md-3 col-xs-12 pull-right" style="margin-top:5px;">
		<div class="input-group custom-search-form">
			<input class="form-control" type="text" placeholder="Search..." name='key' id='key' onchange='readPage(1)'>
			<span class="input-group-btn">
				<button class="btn btn-default" type="button">
				<i class="fa fa-search"></i>
				</button>
			</span>
		</div>
	</div>
	<div class="col-md-2 col-xs-12 pull-right" style="margin-top:5px;">
		<select class="form-control col-md-6 col-xs-12" name='kategori' id='kategori' onchange='readPage(1)'>
			<option value="">PILIH KATEGORI</option>
			<option value="1">INFORMASI</option>
			<option value="2">ARTIKEL</option>
		</select>
	</div>
	<div class="col-md-1 col-xs-12 pull-right" style="margin-top:5px;">
		<select class="form-control col-md-6 col-xs-12" name='limit' id='limit' onchange='readPage(1)'>
			<option value="5">5 rows</option>
			<option value="10">10 rows</option>
			<option value="25">25 rows</option>
			<option value="50">50 rows</option>
			<option value="100">100 rows</option>
		</select>
	</div>
	
</div>

<div class="row" id="dtPost">
	
</div>
<script>
$(document).ready(function() {
	readPage(1);
})

function test(l) {
	var des = document.getElementById('des');
	alert(des);
}
function readPage(page)
{
	
	var limit 	= $('#limit').val();
	var key 	= $('#key').val();
	var kategori= $('#kategori').val();
	
	$.ajax({
		url		: 'post/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key,kategori:kategori},
		success : function(result)
		{
			$('#dtPost').empty().append(result);
		}
	});
}

</script>