

<br/>
<div class="col-md-12 col-xs-12">
	<div class="table-responsive" style='font-size:13px'>
		<table class="table table-bordered table-hovered">
			<thead style='background-color:#5bc0de;'>
				<th style="text-align:center;width:5%">No</th>
				<th style="text-align:center;width:29%">JUDUL</th>
				<th style="text-align:center;width:15%">NIP PENGIRIM</th>
				<th style="text-align:center;width:15%">NAMA PENGIRIM</th>
				<th style="text-align:center;width:8%">TANGGAL</th>
				<th style="text-align:center;width:8%">KATEGORI</th>
				<th style="text-align:center;width:8">STATUS</th>
				<th style="text-align:center;width:13%">AKSI</th>
			</thead>
			<tbody>
				<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
				$no++;
				if($list->num_rows() > 0) { 
					foreach($list->result() as $row) { ?>
					 <tr>
						<td style="text-align:center;"><?php echo $no ?></td>
						<td><?php echo $row->JUDUL ?></td>
						<td><?php echo $row->NIP ?></td>
						<td><?php echo $row->NAMA_PENGIRIM ?></td>
						<td style='text-align:center'><?php echo tgl_indo($row->TGL_INPUT) ?></td>
						<td style="text-align:center;">
							<?php 
								$kat = '';
								if($row->KATEGORI==1) {
									$kat = "Informasi";
									
								} else {
									$kat = "Artikel";
								}
								echo $kat;
							?>
							
						</td>
						<?php if($row->STATUS_POST==1){
							?>
							<td style='text-align:center'><a href='javascript:void(0)' class='btn btn-default btn-xs btn-circle' data-toggle='tooltip' data-placement='top' title='AKTIF'	><i class='fa fa-check'></i></a></td>
						<?php }else
						{ ?>
							<td style='text-align:center' ><a href='javascript:void(0)' class='btn btn-default btn-xs btn-circle' data-toggle='tooltip' data-placement='top' title='TIDAK AKTIF'	><i class='fa fa-remove'></i></a></td>
						<?php }  ?>
						<td style='text-align:center'>
							<a href='<?php echo site_url('home/detail/'.$row->URL.'/html')?>' class='btn btn-success btn-xs btn-circle'   data-toggle='tooltip'  data-placement='top' title='DETAIL : <?php echo $row->JUDUL ?>' ><i class='fa fa-eye' data-toggle='modal' data-target='#detailBarang'></i></a> 
							<?php if($this->session->userdata('level')==2) { ?>
							<a href='<?php echo site_url('post/input/'.encode($row->ID_POST))?>' class='btn btn-primary btn-xs btn-circle' data-toggle='tooltip'  data-placement='top' title='EDIT : <?php echo $row->JUDUL ?>' ><i class='fa fa-edit'></i></a> 
							
							<a href='javascript:void(0)' class='btn btn-danger btn-xs btn-circle' onclick='deletList("<?php echo encode($row->ID_POST);?>","<?php echo $row->IMAGE ?>")' data-toggle='tooltip' data-placement='top' title='Hapus : <?php echo $row->JUDUL ?>'><i class='fa fa-trash'></i></a></td>
							<?php } ?>
					 </tr>
				<?php 	$no++;
					}
				} ?>
				<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
			
			
			</tody>
		</table>
	</div>
	
		<?php echo $paging['list'] ?>
	
</div>

<script>
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})
	
	function deletList(i,x) {
		alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
			if (e) {
				$.ajax({
					url		: "post/delete",
					type	: 'POST',
					dataType: 'json',
					data	: {i:i,x:x},
					
					beforeSend : function()
					{
					   
					},
					success : function(result){
						if(result.rs == '1') {
							readPage($('#current').val());
							alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil dihapus");
						}
						else
						{
							$.sticky('Data gagal dihapus');
						}
					} 
				});
			}else{
			
			}
		});
	}
</script>