<div class="row">
	<div class="col-md-1 col-xs-12" style="margin-top:5px;">
		<?php if($this->session->userdata('level')==2) { ?>
		<span data-toggle="tooltip" data-placement='top' title='Tambah Guru'><a href="<?php echo site_url('galery/input')?>" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Tambah Galeri</a></span>
		<?php } ?>
	</div>
	
	<div class="col-md-3 col-xs-12 pull-right" style="margin-top:5px;">
		<div class="input-group custom-search-form">
			<input class="form-control" type="text" placeholder="Search..." name='key' id='key' onchange='readPage(1)'>
			<span class="input-group-btn">
				<button class="btn btn-default" type="button">
				<i class="fa fa-search"></i>
				</button>
			</span>
		</div>
	</div>
	<div class="col-md-2 col-xs-12 pull-right" style="margin-top:5px;">
		<select class="form-control col-md-6 col-xs-12" name='limit' id='limit' onchange='readPage(1)'>
			<option value="4">4 item</option>
			<option value="8">8 item</option>
			<option value="12">12 item</option>
			<option value="50">16 item</option>
			<option value="20">20 item</option>
		</select>
	</div>
</div>

<div class="row" id="dtGuru">
	
</div>
<script>
$(document).ready(function() {
	readPage(1);
})
function readPage(page)
{
	
	var limit 	= $('#limit').val();
	var key 	= $('#key').val();
	
	$.ajax({
		url		: 'galery/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key},
		success : function(result)
		{
			$('#dtGuru').empty().append(result);
		}
	});
}

</script>