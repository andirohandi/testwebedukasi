

<br/>
	
<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
$no = 0;
if($list->num_rows() > 0) { 
	foreach($list->result() as $row) { 
		
		if($no%4==3) {
			echo "<div class='row'>";
		}
		
		if($row->STATUS_POST==1) {
			$stts = 'Aktif';
		} else {
			$stts = 'Tidak Aktif';
		}
		?>

		<div class='col-md-3 col-sm-1' >
			<div class='panel panel-default'>
			 <img src="<?php echo site_url($row->IMAGE)?>" alt="..." class="img-thumbnail">
			 <div class="panel-body">
				<h4><?php echo $row->JUDUL?></h4><br/>Pengirim :<br/>
				<?php echo $row->NAMA_PENGIRIM."<br/>".$row->NIP."<br/>".$stts?>
				
				<div id='detail<?php echo $no?>' style='display:none'>
					<hr/>
					<?php echo $row->DESKRIPSI;?>
				</div>
			  </div>
			  
			  <div class="panel-footer">
				<table class='table-responsive'>
					<tr>
						<td style='text-align:center;'>
							<a href='javascript:void(0)' id='lihatDetail<?php echo $no?>' class='btn btn-success btn-xs btn-circle'   data-toggle='tooltip'  data-placement='top' title='DETAIL : <?php echo $row->JUDUL ?>' ><i class='fa fa-eye'></i></a> 
							
							<?php if($this->session->userdata('level')==2) { ?>
							<a href='<?php echo site_url('galery/input/'.encode($row->ID_POST))?>' class='btn btn-primary btn-xs btn-circle' data-toggle='tooltip'  data-placement='top' title='EDIT : <?php echo $row->JUDUL ?>' ><i class='fa fa-edit'></i></a> 
							
							<a href='javascript:void(0)' class='btn btn-danger btn-xs btn-circle' onclick='deletList("<?php echo encode($row->ID_POST);?>","<?php echo $row->IMAGE;?>")' data-toggle='tooltip' data-placement='top' title='Hapus : <?php echo $row->JUDUL ?>'><i class='fa fa-trash'></i></a>
							<?php } ?>
						</td>
						<td width='20px'>
						
						</td>
						<td style='color:grey'>
							<?php echo tgl_indo($row->TGL_INPUT)?>
						</td>
					</tr>
				</table>
			  </div>
			</div>
		</div>
		<script>
		$(document).ready(function(){
			$('#lihatDetail<?php echo $no ?>').click(function(){
				$('#detail<?php echo $no ?>').toggle('slow');
			})
		});
		</script>
		<?php 
		if($no%4==3) {
			echo "</div>";
		}
		
	$no++;
	}
} ?>
<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
	
<center><?php echo $paging['list'] ?></center>


<script>
	$(document).ready(function(){
		$('#lihatDetail2').click(function(){
			$('#detail2').toggle('slow');
		})
	});
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})
	
	function deletList(i,x) {
		alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
			if (e) {
				$.ajax({
					url		: "galery/delete",
					type	: 'POST',
					dataType: 'json',
					data	: {i:i,x:x},
					
					beforeSend : function()
					{
					   
					},
					success : function(result){
						if(result.rs == '1') {
							readPage($('#current').val());
							alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil dihapus");
						}
						else
						{
							$.sticky('Data gagal dihapus');
						}
					} 
				});
			}else{
			
			}
		});
	}
</script>