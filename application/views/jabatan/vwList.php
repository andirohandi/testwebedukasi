

<br/>
<div class="col-md-12 col-xs-12">
	<div class="table-responsive" style='font-size:13px'>
		<table class="table table-bordered table-hovered">
			<thead style='background-color:#5bc0de;'>
				<th style="text-align:center;width:">No</th>
				<th style="text-align:center;width:">NAMA JABATAN</th>
				<th style="text-align:center;width:">STATUS</th>
				<th style="text-align:center;width:25%">AKSI</th>
			</thead>
			<tbody>
				<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
				$no++;
				if($list->num_rows() > 0) { 
					foreach($list->result() as $row) { ?>
					 <tr>
						<td style="text-align:center;"><?php echo $no ?></td>
						<td><?php echo $row->JABATAN ?></td>
						<?php if($row->STATUS==1){
							?>
							<td style='text-align:center'><a href='javascript:void(0)' class='btn btn-default btn-xs btn-circle' data-toggle='tooltip' data-placement='top' title='AKTIF'	><i class='fa fa-check'></i></a></td>
						<?php }else
						{ ?>
							<td style='text-align:center' ><a href='javascript:void(0)' class='btn btn-default btn-xs btn-circle' data-toggle='tooltip' data-placement='top' title='TIDAK AKTIF'	><i class='fa fa-remove'></i></a></td>
						<?php }  ?>
						<td style='text-align:center'>
							
							<a href='javascript:void(0)' onclick='editList("<?php echo $row->ID ?>","<?php echo $row->JABATAN ?>","<?php echo $row->STATUS ?>")' class='btn btn-primary btn-xs btn-circle' data-toggle='tooltip'  data-placement='top' title='EDIT' ><i class='fa fa-edit'></i></a> 
							
							<a href='javascript:void(0)' class='btn btn-danger btn-xs btn-circle' onclick='deletList("<?php echo $row->ID;?>")' data-toggle='tooltip' data-placement='top' title='Hapus'><i class='fa fa-trash'></i></a></td>
							
					 </tr>
				<?php 	$no++;
					}
				} ?>
				<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
			
			
			</tody>
		</table>
	</div>
	
		<?php echo $paging['list'] ?>
	
</div>

<script>
$(function () {
	$('[data-toggle=\"tooltip\"]').tooltip()
})

function deletList(i) {
	alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
		if (e) {
			$.ajax({
				url		: "jabatan/delete",
				type	: 'POST',
				dataType: 'json',
				data	: {i:i},
				
				beforeSend : function()
				{
				   
				},
				success : function(result){
					if(result.rs == '1') {
						readPage($('#current').val());
						alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil dihapus");
					}
					else
					{
						alertify.alert('Data gagal dihapus. Data masih digunakan oleh user');
					}
				} 
			});
		}else{
		
		}
	});
}

function editList(x,y,z) {
	$('#nama_jabatan').val(y);
	$('#id_jabatan').val(x);
	$('#status').val(z);
	var button = "<button type='button' name='edit' id='edit' onclick='edit()' class='btn btn-primary pull-right'><i class='fa fa-check'></i> Edit Jabatan</button>";
	$('#but').html(button);
}

function edit() {
	
	var nama = $('#nama_jabatan').val();
	var id = $('#id_jabatan').val();
	var status = $('#status').val();
	var but		= "<button type='button' name='simpan' id='simpan' onclick='simpan()' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan Jabatan</button>";
	$.ajax({
		url		: "jabatan/update",
		type	: 'POST',
		dataType: 'json',
		data	: {nama:nama,id:id,status:status},
		
		beforeSend : function()
		{
		   
		},
		success : function(result){
			if(result.rs == '1') {
				readPage($('#current').val());
				$('#but').html(but);
				$('#nama_jabatan').val('');
				$('#status').val(1);
				alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil diubah");
			}
			else
			{
				alertify.alert('Data gagal diubah');
			}
		} 
	});
}
</script>