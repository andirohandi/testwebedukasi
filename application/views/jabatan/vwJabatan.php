<style>
.error{
	color: #FB3A3A;
    padding: 0;
    text-align: left;
}
#kanan-menu{
	float:right;

}

</style>

<div class="row">
	<div class="col-md-8 col-xs-12">
		<div class="col-md-2 col-xs-12" style="margin-top:5px;margin-left:-15px">
			<select class="form-control col-md-6 col-xs-12" name='limit' id='limit' onchange='readPage(1)'>
				<option value="5">5 rows</option>
				<option value="10">10 rows</option>
				<option value="25">25 rows</option>
				<option value="50">50 rows</option>
				<option value="100">100 rows</option>
			</select>
		</div>
		<div class="col-md-6 col-xs-12" style="margin-top:5px;">
		</div>
		
		<div class="col-md-4col-xs-12" style="margin-top:5px;">
			<div class="input-group custom-search-form">
				<input class="form-control" type="text" placeholder="Search..." name='key' id='key' onchange='readPage(1)'>
				<span class="input-group-btn">
					<button class="btn btn-default" type="button">
					<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</div>
		<div class="row" id="dtJabatan">
			
		</div>
	</div>
	<div class="col-md-4 col-xs-12" style="margin-top:5px;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class='glyphicon glyphicon-plus'></i> Tambah Data Jabatan</h3>
			</div>
			<div class="panel-body">
				<br/>
				<div class="form-group">
					<label>Nama Jabatan</label>
					<input type="text" class="form-control" id="nama_jabatan" name="nama_jabatan" value="" required  placeholder='Masukkan nama jabatan . .' ><input type="hidden" class="form-control" id="id_jabatan" name="id_jabatan" value="" required  placeholder='Masukkan nama jabatan . .' >
				</div>
				<div class="form-group">
						<label>Status</label>
						<select id="status" name="status" class='form-control' required>
							<option value='1'>AKTIF</option>
							<option value='0'>TIDAK AKTIF</option>
						</select>
					</div>
				<br/>
				<div class="row">
					<div class="col-md-12 col-sm-12" id="but">
						<button type="button" name='simpan' id='simpan' onclick='simpan()' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan Jabatan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	readPage(1);
})
function readPage(page)
{
	
	var limit 	= $('#limit').val();
	var key 	= $('#key').val();
	
	$.ajax({
		url		: 'jabatan/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key},
		success : function(result)
		{
			$('#dtJabatan').empty().append(result);
		}
	});
}

function simpan() {
	var nama_jabatan= $('#nama_jabatan').val();
	var status 		= $('#status').val();
	
	$.ajax({
		url		: 'jabatan/create',
		type	: 'POST',
		dataType: 'json',
		data	: {nama_jabatan:nama_jabatan,status:status},
		success : function(result)
		{
			if(result.rs==1) {
				alertify.success("<i clas=='gliphycon gliphycon-check'></i> Data Berhasil Disimpan");
				readPage($('#current').val());
			} else {
				alertify.alert("Inputan anda belum lengakap. Periksa kembali inputan anda");
			}
		}
	});
	
}


</script>