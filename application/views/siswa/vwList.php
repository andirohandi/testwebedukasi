

<br/>
<div class="col-md-12 col-xs-12">
	<div class="table-responsive" style='font-size:13px'>
		<table class="table table-bordered table-hovered">
			<thead style='background-color:#5bc0de;'>
				<th style="text-align:center;width:">No</th>
				<th style="text-align:center;width:">NIS</th>
				<th style="text-align:center;width:">NAMA</th>
				<th style="text-align:center;width:">JENIS KELAMIN</th>
				<th style="text-align:center;width:">ALAMAT</th>
				<th style="text-align:center;width:">TGL LAHIR</th>
				<th style="text-align:center;width:">NO HP</th>
				<th style="text-align:center;width:">EMAIL</th>
				<th style="text-align:center;width:">STATUS</th>
				<th style="text-align:center;width:13%">AKSI</th>
			</thead>
			<tbody>
				<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
				$no++;
				if($list->num_rows() > 0) { 
					foreach($list->result() as $row) { ?>
					 <tr>
						<td style="text-align:center;"><?php echo $no ?></td>
						<td><?php echo $row->NIS ?></td>
						<td><?php echo $row->NAMA ?></td>
						<td style='text-align:center'><?php if($row->JENIS_KELAMIN==1){ echo "Laki-Laki";}else{ echo "Perempuan";} ?></td>
						<td><?php echo $row->ALAMAT ?></td>
						<td style='text-align:center'><?php echo tgl_indo($row->TGL_LAHIR) ?></td>
						<td><?php echo $row->NO_HP ?></td>
						<td><?php echo $row->EMAIL ?></td>
						<?php if($row->STATUS==1){
							?>
							<td style='text-align:center'><a href='javascript:void(0)' class='btn btn-default btn-xs btn-circle' data-toggle='tooltip' data-placement='top' title='AKTIF'	><i class='fa fa-check'></i></a></td>
						<?php }else
						{ ?>
							<td style='text-align:center' ><a href='javascript:void(0)' class='btn btn-default btn-xs btn-circle' data-toggle='tooltip' data-placement='top' title='TIDAK AKTIF'	><i class='fa fa-remove'></i></a></td>
						<?php }  ?>
						<td style='text-align:center'>
							<a href='<?php echo site_url('siswa/detail/'.$row->ID)?>' class='btn btn-success btn-xs btn-circle'   data-toggle='tooltip'  data-placement='top' title='DETAIL : <?php echo $row->NAMA ?>' ><i class='fa fa-eye' data-toggle='modal' data-target='#detailBarang'></i></a> 
							
							<a href='<?php echo site_url('siswa/input/'.encode($row->ID))?>' class='btn btn-primary btn-xs btn-circle' data-toggle='tooltip'  data-placement='top' title='EDIT : <?php echo $row->NAMA ?>' ><i class='fa fa-edit'></i></a> 
							
							<a href='javascript:void(0)' class='btn btn-danger btn-xs btn-circle' onclick='deletList("<?php echo encode($row->ID);?>","<?php echo $row->IMAGE;?>")' data-toggle='tooltip' data-placement='top' title='Hapus : <?php echo $row->NAMA ?>'><i class='fa fa-trash'></i></a></td>
					 </tr>
				<?php 	$no++;
					}
				} ?>
				<input type='hidden' id='current' name='current' value='<?php echo $paging['current'] ?>'>
			</tody>
		</table>
	</div>
	
		<?php echo $paging['list'] ?>
	
</div>

<script>
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})
	
	function deletList(i,x) {
		alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
			if (e) {
				$.ajax({
					url		: "siswa/delete",
					type	: 'POST',
					dataType: 'json',
					data	: {i:i,x:x},
					
					beforeSend : function()
					{
					   
					},
					success : function(result){
						if(result.rs == '1') {
							readPage(1);
							alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil dihapus");
						}
						else
						{
							$.sticky('Data gagal dihapus');
						}
					} 
				});
			}else{
			
			}
		});
	}
</script>