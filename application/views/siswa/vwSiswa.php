<style>
.error{
	color: #FB3A3A;
    padding: 0;
    text-align: left;
}
#kanan-menu{
	float:right;

}
</style>

<div class="row">
	<div class="col-md-1 col-xs-12" style="margin-top:5px;">
		<span data-toggle="tooltip" data-placement='top' title='Tambah Guru'><a href="<?php echo site_url('siswa/input')?>" class="btn btn-success btn-sm btn-circle" ><i class="fa fa-plus"></i></a></span>
	</div>
	
	<div class="col-md-3 col-xs-12 pull-right" style="margin-top:5px;">
		<div class="input-group custom-search-form">
			<input class="form-control" type="text" placeholder="Search..." name='key' id='key' onchange='readPage(1)'>
			<span class="input-group-btn">
				<button class="btn btn-default" type="button">
				<i class="fa fa-search"></i>
				</button>
			</span>
		</div>
	</div>
	<div class="col-md-1 col-xs-12 pull-right" style="margin-top:5px;">
		<select class="form-control col-md-6 col-xs-12" name='limit' id='limit' onchange='readPage(1)'>
			<option value="5">5 rows</option>
			<option value="10">10 rows</option>
			<option value="25">25 rows</option>
			<option value="50">50 rows</option>
			<option value="100">100 rows</option>
		</select>
	</div>
</div>

<div class="row" id="dtGuru">
	
</div>
<script>
$(document).ready(function() {
	readPage(1);
})
function readPage(page)
{
	
	var limit 	= $('#limit').val();
	var key 	= $('#key').val();
	
	$.ajax({
		url		: 'siswa/read/'+page,
		type	: 'POST',
		dataType: 'html',
		data	: {limit:limit,key:key},
		success : function(result)
		{
			$('#dtGuru').empty().append(result);
		}
	});
}

</script>