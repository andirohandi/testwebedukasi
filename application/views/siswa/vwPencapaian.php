

<div class="row">
	<div class="col-md-8 col-xs-12">
		<div class="row" id="dtPencapaian">
			
		</div>
	</div>
	<div class="col-md-4 col-xs-12" style="margin-top:5px;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class='glyphicon glyphicon-plus'></i> Tambah Pencapaian</h3>
			</div>
			<div class="panel-body">
				<br/>
				<div class="form-group">
					<label>Tahun</label>
					<select id="tahun" name="tahun" class='form-control' required>
						<?php 
							$thn = (int) date('Y'); 
							$bts = $thn - 20;
							
							for($i=$thn; $i>=$bts; $i--) {
								echo "<option value='$i'>$i</option>";
							}
						?>
					</select>
				</div>
				<div class="form-group">
					<label>Pencapaian</label>
					<input type="text" class="form-control" id="pencapaian" name="pencapaian" value="" required  placeholder='Masukkan Pencapaian..' ><input type="hidden" class="form-control" id="id_pencapaian" name="id_pencapaian" value="" required >
				</div>
				
				<br/>
				<div class="row">
					<div class="col-md-12 col-sm-12" id="but">
						<button type="button" name='simpan' id='simpan' onclick='simpan()' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan Pencapaian</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	readPagePen();
	
})
function readPagePen()
{
	
	$.ajax({
		url		: 'readPencapaian/',
		type	: 'POST',
		dataType: 'html',
		data	: {},
		success : function(result)
		{
			$('#dtPencapaian').empty().append(result);
		}
	});
}


</script>