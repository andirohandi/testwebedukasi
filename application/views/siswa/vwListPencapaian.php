

<br/>
<div class="col-md-12 col-xs-12">
	<div class="table-responsive" style='font-size:13px'>
		<table class="table table-bordered table-hovered">
			<thead style='background-color:#5bc0de;'>
				<th style="text-align:center;width:">No</th>
				<th style="text-align:center;width:">TAHUN</th>
				<th style="text-align:center;width:">PENCAPAIAN</th>
				<th style="text-align:center;width:25%">AKSI</th>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				if($list->num_rows() > 0) { 
					foreach($list->result() as $row) { ?>
					 <tr>
						<td style="text-align:center;"><?php echo $no ?></td>
						<td style="text-align:center;" ><?php echo $row->TAHUN ?></td>
						<td><?php echo $row->PENCAPAIAN ?></td>
						<td style='text-align:center'>
							
							<a href='javascript:void(0)' onclick='editListPencapaian("<?php echo $row->ID ?>","<?php echo $row->TAHUN ?>","<?php echo $row->PENCAPAIAN ?>")' class='btn btn-primary btn-xs btn-circle' data-toggle='tooltip'  data-placement='top' title='EDIT' ><i class='fa fa-edit'></i></a> 
							
							<a href='javascript:void(0)' class='btn btn-danger btn-xs btn-circle' onclick='deletList("<?php echo encode($row->ID);?>")' data-toggle='tooltip' data-placement='top' title='Hapus'><i class='fa fa-trash'></i></a></td>
							
					 </tr>
				<?php 	$no++;
					}
				} ?>
			
			
			</tody>
		</table>
	</div>
</div>
<script>
	$(function () {
		$('[data-toggle=\"tooltip\"]').tooltip()
	})
	
	function deletList(i) {
		alertify.confirm("Apakah Anda Yakin Akan Menghapus Data ini ?", function (e) {
			if (e) {
				$.ajax({
					url		: "deletePencapaian",
					type	: 'POST',
					dataType: 'json',
					data	: {i:i},
					
					beforeSend : function()
					{
					   
					},
					success : function(result){
						if(result.rs == '1') {
							readPagePen();
							alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil dihapus");
						}
						else
						{
							$.sticky('Data gagal dihapus');
						}
					} 
				});
			}else{
			
			}
		});
	}
	
	function editListPencapaian(x,y,z) {
		$('#id_pencapaian').val(x);
		$('#pencapaian').val(z);
		$('#tahun').val(y);
		var button = "<button type='button' name='edit' id='edit' onclick='edit()' class='btn btn-primary pull-right'><i class='fa fa-check'></i> Edit Jabatan</button>";
		$('#but').html(button);
	}
	
	function edit() {
	
		var pencapaian = $('#pencapaian').val();
		var id = $('#id_pencapaian').val();
		var tahun = $('#tahun').val();
		var but		= "<button type='button' name='simpan' id='simpan' onclick='simpan()' class='btn btn-success pull-right'><i class='fa fa-check'></i> Simpan Pencapaian</button>";
		$.ajax({
			url		: "updatePencapaian",
			type	: 'POST',
			dataType: 'json',
			data	: {pencapaian:pencapaian,id:id,tahun:tahun},
			
			beforeSend : function()
			{
			   
			},
			success : function(result){
				if(result.rs == '1') {
					readPagePen();
					$('#but').html(but);
					$('#tahun').val(2015);
					$('#pencapaian').val('');
					alertify.success("<i class='glyphicon glyphicon-ok' ></i> Data berhasil diubah");
				}
				else
				{
					alertify.alert('Data gagal diubah');
				}
			} 
		});
	}
	
	
function simpan() {
	var pencapaian  = $('#pencapaian').val();
	var tahun 		= $('#tahun').val();
	
	$.ajax({
		url		: 'createPencapaian',
		type	: 'POST',
		dataType: 'json',
		data	: {pencapaian:pencapaian,tahun:tahun},
		success : function(result)
		{
			if(result.rs==1) {
				alertify.success("<i clas=='gliphycon gliphycon-check'></i> Data Berhasil Disimpan");
				$('#pencapaian').val('');
				readPagePen();
			} else if(result.rs==2) {
				alertify.alert("<i clas=='gliphycon gliphycon-check'></i> Tahun pencapaian sudah ada");
			} else {
				alertify.alert("Inputan anda belum lengakap. Periksa kembali inputan anda");
			}
		}
	});
	
}

</script>