<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title ?></title>

	<link href="<?php base_url()?>assets/front/logo.png" rel="shortcut icon" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo HTTP_BACK ?>bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo HTTP_BACK ?>css/mystyle.css" rel="stylesheet">
	
	<!-- Alertiffy --!-->
	
	<link href="<?php echo base_url();?>assets/alertify/css/alertify.core.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/alertify/css/alertify.default.css" rel="stylesheet">
	
    <!-- MetisMenu CSS -->
    <link href="<?php echo HTTP_BACK ?>bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo HTTP_BACK ?>dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo HTTP_BACK ?>dist/css/sb-admin-2.css" rel="stylesheet">
    
    <!-- Morris Charts CSS -->
    <link href="<?php echo HTTP_BACK ?>bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo HTTP_BACK ?>bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Custom Table -->
	<link rel="stylesheet" href="<?php echo HTTP_BACK ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?php echo HTTP_BACK ?>bower_components/datatables-responsive/css/dataTables.responsive.css">

	<!-- jQuery -->
    <script src="<?php echo HTTP_BACK ?>bower_components/jquery/dist/jquery.min.js"></script>
	
	<!-- JQUEY Validate !-->
	<script src="<?php echo base_url() ?>assets/js/jquey_validate.js"></script>
</head>

<body style='padding-top:70px'>

    <div id="wrapper" style="background-color:">
        <nav class="navbar navbar-default navbar-static-top navbar-fixed-top" role="navigation" style="margin-bottom: 0;background-color:;" >
            <?php $this->load->view('layout/vwHeader')?>
            <?php $this->load->view('layout/vwSidebar')?>
        </nav>
        <div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header"><?php echo $titlepage?></h1>
				</div>
			</div>
            <?php $this->load->view($view)?>
        </div>
    </div>
   
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo HTTP_BACK ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/alertify/js/alertify.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo HTTP_BACK ?>bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!--script src="<?php echo HTTP_BACK ?>bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo HTTP_BACK ?>bower_components/morrisjs/morris.min.js"></script>
    <script src="<?php echo HTTP_BACK ?>js/morris-data.js"></script--!-->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo HTTP_BACK ?>dist/js/sb-admin-2.js"></script>

	 <script>
		$('.tooltip-demo').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		})
		$('.tooltip-dm').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		})
	</script>
</body>
</html>
