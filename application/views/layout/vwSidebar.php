<?php 
			
	$lvl = $this->session->userdata('level');
	if($lvl==2) {
		$list = $this->m_guru->getProfile($this->session->userdata('id'));
	} else {
		$list = $this->m_siswa->getProfile($this->session->userdata('id'));
	}
?>

<div class="navbar-default sidebar" role="navigation" style="background-color:">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<?php if($lvl!=1) { ?>
			<li class="sidebar-search">
				<img src="<?php echo base_url($list['IMAGE'])?>" class="img-thumbnail" width="20%"/><?php echo " Hi, ";if($this->session->userdata('level')== 1) echo $list['NAMA']; else echo $this->session->userdata('nama')?>
				<!-- /input-group -->
			</li>
			
			<?php } if($this->session->userdata('level')!= 1) {
				?>
					<li>
						<a href="<?php echo site_url('profile')?>"><i class="fa fa-user"></i> Profile</a>
					</li>
				<?php
			}
			
			
			if($this->session->userdata('level')== 1) {
				?>
					<li>
						<a href="<?php echo site_url('adm/dashboard')?>"><i class="fa fa-dashboard fa-fw"></i> Panel Admin</a>
					</li>
					<li>
						<a href="#"><i class="fa fa-folder-open fa-fw"></i> Master Data<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li>
								<a href="<?php echo site_url('guru')?>">Data Guru</a>
							</li>
							<li>
								<a href="<?php echo site_url('siswa')?>">Data Siswa</a>
							</li>
							<li>
								<a href="<?php echo site_url('jabatan')?>">Data Jabatan</a>
							</li>
						</ul>
					</li>
				<?php
			}
			
			if($this->session->userdata('level')!= 3) {
				
				?>
					<li>
						<a href="<?php echo site_url('post')?>"><i class="fa fa-folder-open fa-fw"></i> Postingan</a>
					</li>
					<li>
						<a href="<?php echo site_url('galery')?>"><i class="fa fa-folder-open fa-fw"></i> Galeri</a>
					</li>
					<li>
						<a href="<?php echo site_url('forum')?>"><i class="fa fa-folder-open fa-fw"></i> Forum</a>
					</li>
				<?php
				
			}

			if($this->session->userdata('level')== 3) {
				
				?>
					<li>
						<a href="<?php echo site_url('siswa/pencapaian')?>"><i class="fa fa-folder-open fa-fw"></i> Pencapaian</a>
					</li>
					<li>
						<a href="<?php echo site_url('siswa/history')?>"><i class="fa fa-folder-open fa-fw"></i> Log History</a>
					</li>
				<?php
			}
			?>
			
			<?php
			if($this->session->userdata('level')== 1) {
				?>

					<li>
						<a href="<?php echo site_url('front')?>"><i class="fa fa-gear"></i> Setting</a>
					</li>
				
				<?php
			} ?>
		</ul>
	</div>
</div>