<div class='blue'>
	<div class='container hide-on-med-and-down' >
		<div class='row'>
			<div class='col s12 m1 l1'>
				<img src='<?php echo base_url()?>/assets/front/logo.png' /><br/>
			</div>
			<div class='col s12 m11 l11'>
				<h3 class='' style='font-family:calliberi;float:left;text-shadow:3px 1px 2px white;'><span>&nbsp;&nbsp;SMA Negri 2 Bandung<span></h3>
			</div>
		</div>
	</div>
</div>

<ul id="profile" class="dropdown-content blue">
  <li><a href="<?php echo base_url('home/about')?>">About</a></li>
  <li class="divider"></li>
  <li><a href="<?php echo base_url('home/visimisi')?>">Visi Misi</a></li>
  
</ul>

<nav id="explain" style='z-index:9999;margin-top:-20px'>
	<div class="nav-wrapper  blue accent-4 z-depth-1 " >
		<div class='row'>
			<div class='col s12 m1 l1 right'>
				<div class="hide-on-med-and-down">
					<a class="btn-floating btn-large blue waves-effect waves-light">
						<i class="large material-icons">perm_identity</i>
					</a>
				</div>
			</div>
			<div class='col s12 m8 l8 right'>
				<a href="#" class="brand-logo"></a>
				<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li class='waves-effect waves-green <?php echo $pg == 'home' ? 'active':''?>'><a href="<?php echo site_url('')?>">Home</a></li>
					<li class='dropdown-button <?php echo $pg == 'profile' ? 'active':''?>' data-activates="profile" ><a href="javascript:void(0)" >Profile<i class="material-icons right">arrow_drop_down</i></a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'galeri' ? 'active':''?>'><a href="<?php echo site_url('home/kategori/3/gallery')?>">Galerry</a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'informasi' ? 'active':''?>'><a href="<?php echo site_url('home/kategori/1/informations')?>">Informations</a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'artikel' ? 'active':''?>' ><a href="<?php echo site_url('home/kategori/2/articles')?>">Articles</a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'forum' ? 'active':''?>'><a href="<?php echo site_url('home/forum')?>" >Forum</a></li>
					<?php 
						if($this->session->userdata('logedIn')) { ?>
							<li class='waves-effect waves-green' ><a href="<?php echo site_url('adm/dashboard')?>">Panel Admin</a></li>
						<?php	
						}
					?>
					
				</ul>
				<div class='hide-on-large-only'><span style='font-family:callibri;font-size:20px'><strong>SMA Negri 1 Bandung</strong></span></div>
				<ul id="mobile-demo" class="side-nav">
					<?php 
						if($this->session->userdata('logedIn')) { ?>
							<li class='waves-effect waves-green' ><a href="<?php echo site_url('adm/dashboard')?>">Panel Admin</a></li>
						<?php	
						}
					?>
					<li class='waves-effect waves-green <?php echo $pg == 'home' ? 'active':''?>'><a href="<?php echo site_url('')?>">Home</a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'galeri' ? 'active':''?>'><a href="<?php echo site_url('home/kategori/3/gallery')?>">Galerry</a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'informasi' ? 'active':''?>'><a href="<?php echo site_url('home/kategori/1/informations')?>">Informations</a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'artikel' ? 'active':''?>' ><a href="<?php echo site_url('home/kategori/2/articles')?>">Articles</a></li>
					<li class='waves-effect waves-green <?php echo $pg == 'forum' ? 'active':''?>'><a href="<?php echo site_url('home/forum')?>" >Forum</a></li>
					<?php
						if($this->session->userdata('level') == 1) {
							$u = 'adm/doLogout';
						} else {
							$u = 'adm/doLogoutUser';
						}
						
						if($this->session->userdata('logedIn')) { ?>
							<li class='waves-effect waves-green'><a href="<?php echo site_url($u)?>">Log Out</a></li>
						<?php }
					
					?>
				</ul>
			</div>
			
		</div>
	</div>
</nav>	
<script>
$(document).ready(function(){
	$(".dropdown-button").dropdown();
})

</script>