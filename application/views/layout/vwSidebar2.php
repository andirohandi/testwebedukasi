<div class='col s12 m3 l3' style='font-size:12px'>
	<div class='card-panel  blue'>
		<b><?php 
			if($this->session->userdata('logedIn')) {
				echo "USERS LOGOUT";
			} else {
				echo "USERS LOGIN";
			}
		?></b>
	</div>
	<ul class="collection hoverable z-depth-1" style='margin-top:-12px'>
		<?php 
		if(!$this->session->userdata('logedIn')) { ?>
		<li class="collection-item">
			<div class="row">
				<center><?php echo validation_errors(); ?></center>
				<form class="col s12" name='formLogin' arole="form" action="<?php echo site_url('adm/doLoginUser')?>" method="POST">
				  <div class="row">
					<div class="input-field col s12 l12 m12">
					  <i class="material-icons prefix">account_circle</i>
					  <input id="username" name='username' type="text" class="validate" required >
					  <label for="username" >Username</label>
					</div>
				  </div>
				  <div class="row" style='margin-top:-100px'>
					<div class="input-field col s12 m12 l12">
					  <i class="material-icons prefix">vpn_key</i>
					  <input id="icon_telephone" type="password" class="validate" id='password' name='password' required >
					  <label for="">Password</label>
					</div>
				  </div>
				  <center><button class="waves-effect waves-light btn" type='submit'><i class="material-icons left">input</i>login</button></center>
				</form>
			</div>
		</li>
		<?php } else { ?>
		<li class="collection-item">
			<center><a href='<?php echo site_url('adm/doLogoutUser')?>' class="waves-effect waves-light btn"><i class="material-icons left">input</i>Logout</a></center>
		</li>
		<?php } ?>
	 </ul>
	
	<div class='card-panel  blue'>
		<b>LATEST FORUM</b>
	</div>
	<ul class="collection hoverable z-depth-1" style='margin-top:-12px'>
		<?php 
			foreach($this->m_forum->getSelect('',5,'')->result() as $rows) {
			?>
				<li class="collection-item waves-effect waves-green "><a href='<?php echo site_url('home/detailForum/'.$rows->ID.'/'.$rows->URL.'.html')?>' ><?php echo $rows->JUDUL?></a></li>
			<?php }
		?>
	 </ul>
	 
	<div class='card-panel  blue'>
		<b>LATEST INFORMATIONS</b>
	</div>
	<ul class="collection hoverable z-depth-1" style='margin-top:-12px'>
		<?php 
			foreach($this->m_post->article(array('KATEGORI'=>1))->result() as $rows) {
			?>
				<li class="collection-item waves-effect waves-green "><a href='<?php echo site_url('home/detail/'.$rows->URL.'.html')?>' ><?php echo $rows->JUDUL?></a></li>
			<?php }
		?>
	 </ul>
	 
	<div class='card-panel  blue'>
		<b>LATEST ARTYCLES</b>
	</div>
	<ul class="collection hoverable z-depth-1" style='margin-top:-12px'>
		<?php 
			foreach($this->m_post->article(array('KATEGORI'=>2))->result() as $rows) {
			?>
				<li class="collection-item waves-effect waves-green "><a href='<?php echo site_url('home/detail/'.$rows->URL.'.html')?>' ><?php echo $rows->JUDUL?></a></li>
			<?php }
		?>
	 </ul>
</div>