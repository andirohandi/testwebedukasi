<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<a class="navbar-brand" href="<?php echo site_url('ab/admin')?>">SMA Negri 2 Bandung</a>
</div>

<ul class="nav navbar-top-links navbar-right">
	<li class="tooltip-demo">
		<a href="<?php echo site_url('home')?>" title="" data-placement="left" data-toggle="tooltip" data-original-title="Kunjungi Web" >
			<i class="fa fa-globe fa-fw"></i>  
		</a>
	</li>
	
	<?php 
		if($this->session->userdata('level')==1) {
			$ur = 'adm/doLogout';
		} else {
			$ur = 'adm/doLogoutUser';
		}
	
	?>
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
		</a>
		<ul class="dropdown-menu dropdown-user">
			<li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
			</li>
			<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
			</li>
			<li class="divider"></li>
			<li><a href="<?php echo site_url($ur)?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
			</li>
		</ul>
	</li>
</ul>
