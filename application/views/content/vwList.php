<?php $rs = $this->m_front->getSelect();?>
<div class='col s12 m9 l9 '>
	<div class='row'>
		<div class='col s12 m12 l12 card'>
			<div class="card z-depth-0 hoverable">
				<div class="card-image">
					<?php 
						foreach($rs->result() as $row) {
							?>
								<img src="<?php echo base_url($row->IMAGE_HOME)?>">
							<?php 
						}
					?>
					<span class="card-title"></span>
				</div>
			</div>
		</div>
		<?php $this->load->view('content/vwListAll')?>
	</div>
</div>