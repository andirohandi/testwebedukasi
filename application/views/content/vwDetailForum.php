<?php foreach($list->result() as $row) {
	
	?>
<div class='col s12 m9 l9 '>
	<div class='row'>
		<div class="card-panel grey lighten-5 z-depth-1 hoverable">
			<div class='row'>
				<div class="col s12 m12 l12">
					FORUM : <span style='font-weight:bold' class='right'><?php echo "Tema : ".strtoupper($row->JUDUL)?></span>
					<div class='card-panel blue'>
						<span style='font-weight:bold;color:white'><?php echo $row->NIP." : ".$row->NAMA ?></span><br/>
						<span style='font-size:12px;color:white'><?php echo $row->EMAIL?></span><div class='right' style='font-size:12px;color:white'><?php echo tgl_indo($row->TGL_INPUT) ?></div>
					<hr/>
						<?php echo $row->DESKRIPSI;
						if($this->session->userdata('username') == $row->NIP) { ?>
							<a style='color:white' href='<?php echo site_url('forum/input/'.encode($row->ID))?>'>Edit </a>
						<?php } else{
							
						}
						?>
						
					</div>
					<br/>
					Coments :
					<?php
						$no = 1;
						foreach($this->m_forum->getComent($id)->result() as $rows) {
							if($no%2==0) {
								
							
							?>
								<div class='card-panel blue lighten-3'>
									<div class='right' style='font-weight:bold;'><?php echo $rows->PENGIRIM." : ".$rows->NO_INDUK; ?></div><br/>
									<div class='right' style='font-size:12px;color:grey'><?php echo tgl_indo($rows->TGL_INPUT) ?></div><br/>
									<?php echo $rows->KOMENTAR;
									if($this->session->userdata('username') == $rows->NO_INDUK) {
									?>
									<br/><br/><a href='javascript:void(0)' onclick="hapus('<?php echo $rows->ID?>')">Delete</a>
									<?php } else {
										
									} ?>
									
								</div>
							
							<?php
							} else {
								?>
								<div class='card-panel blue lighten-4'>
									<div class='right' style='font-weight:bold;'><?php echo $rows->PENGIRIM." : ".$rows->NO_INDUK; ?></div><br/>
									<div class='right' style='font-size:12px;color:grey'><?php echo tgl_indo($rows->TGL_INPUT) ?></div><br/>
									<?php echo $rows->KOMENTAR;
									if($this->session->userdata('username') == $rows->NO_INDUK) {
									?>
									<br/><br/><a href='javascript:void(0)' onclick="hapus('<?php echo $rows->ID?>')">Delete</a>
									<?php } else {
										
									} ?>
									
								</div>
								<?php
							}
						$no++;
						}
					
					?>
					
					<br/>
					
					<div class="input-field col s12" id='textComent'>
						 <i class="material-icons prefix">mode_edit</i>
						<textarea id="koment" class="materialize-textarea" <?php if(!$this->session->userdata('logedIn')){ echo "readonly";} ?> ></textarea>
						<label for="koment"><?php if($this->session->userdata('logedIn')){ echo "Add Your Comment Here : ";}
						else { echo "Logint Firs to Comment.."; }?></label>
						<a class="waves-effect waves-light btn blue right" onclick='simpan()'><i class="material-icons left">check</i>Share</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function simpan() {
	var komentar = $('#koment').val();
	var id_forum = '<?php echo $id ?>';
	
	if(komentar=='') {
		alert('You have to add a coments.')
	} else {
		$.ajax({
			url : '<?php echo base_url()?>home/createComent',
			data : {komentar:komentar,id_forum:id_forum},
			type : 'POST',
			dataType : 'JSON',
			beforeSend : function() {
				$('#textComent').empty().append("<div class='preloader-wrapper big active center'><div class='spinner-layer spinner-blue-only'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
			},
			success : function(data) {
				$('#koment').val('');
				location.reload();
			}
				
		});
	}
}

function hapus(i) {
	$.ajax({
			url : '<?php echo base_url()?>home/deleteComent',
			data : {i:i},
			type : 'POST',
			dataType : 'JSON',
			beforeSend : function() {
				
			},
			success : function(data) {
				alert('Deleted has success')
				location.reload();
			}
				
		});
}
</script>

<?php } ?> 
