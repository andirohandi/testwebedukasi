<?php foreach($list->result() as $row) {
	
	if($row->KATEGORI==1) {
		$kat = 'I';
		$kat2 = 'Infromations';
	} else {
		$kat = 'A';
		$kat2 = 'Articles';
	}?>
<div class='col s12 m9 l9 '>
	<div class='row'>
			<div class='card-panel blue z-depth-1 hoverable'>
					<span style='font-weight:bold;color:white'><?php echo $row->NIP." : ".$row->NAMA_PENGIRIM ?></span><br/>
					<span style='font-weight:bold;color:white'><?php echo $row->EMAIL ?></span><br/>
			</div>
			<div class='card-panel col s12 m12 l12 hoverable' style='min-height:700px;margin-top:-10px'>
				<h5><small><i class="tiny material-icons">toc</i> <b><?php echo strtoupper($row->JUDUL); ?></b></small><a class="btn-floating btn-large blue waves-effect waves-light right"><?php echo $kat?></a></h5>
				
				<span style='font-size:12px;color:grey'>Date : <?php echo tgl_indo($row->TGL_INPUT)?> | Category : <a href='<?php echo site_url('home/kategori/'.$row->KATEGORI.'/'.$kat2)?>'><?php echo $kat2?></a></span>
				<hr/>
				<br/>
				<?php
				
				if($row->IMAGE != 'uploads/img/no_image.png') {
					?><center><img  class='responsive-img hoverable' src='<?php echo base_url($row->IMAGE); ?>'></center>
				<?php }
				 echo $row->DESKRIPSI; ?>
				<br/><br/>
			</div>
		</div>
</div>

<?php } ?> 
