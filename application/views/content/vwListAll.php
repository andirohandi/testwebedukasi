<br/>
<?php $no = ($paging['limit']*$paging['current'])-$paging['limit'];
		
		if($list->num_rows() > 0) { 
			$nom = 1;
			foreach($list->result() as $row) {
					$kat = '';
					if($row->KATEGORI==1) {
						$kat = "Informasi";
					} else if($row->KATEGORI==2){
						$kat = "Artikel";
					}else {
						$kat = "Gallery";
					}
					
				if($row->KATEGORI==3) {
					
					if($nom%3==0) {
						echo "<div class='row'>";
					}
						
					
					?>
					
					<div class='col s12 m4 l4'>
						<div class="card  hoverable">
							<div class="card-image waves-effect waves-block waves-light">
							  <img class="activator" src="<?php echo base_url($row->IMAGE)?>">
							</div>
							<div class="card-content">
							  <span class="card-title activator grey-text text-darken-4" style='font-size:15px'><?php echo $row->JUDUL; ?><i class="material-icons right">more_vert</i></span>
							  <p><a href="#"><?php echo tgl_indo($row->TGL_INPUT); ?></a></p>
							</div>
							<div class="card-reveal">
							  <span class="card-title grey-text text-darken-4" style='font-size:15px'><?php echo $row->NAMA_PENGIRIM; ?><i class="material-icons right">close</i></span>
								<br/>Deskripsi : <br/><?php echo $row->DESKRIPSI;?>
							</div>
						</div>
					</div>
					<?php
					if($nom%3==0) {
						echo "</div>";
					}
					
					
					
				} else {
				?>
				<div class='col s12 m12 l12 card-panel hoverable' >
					<h5 style='font-weight:bold'><small><i class="tiny material-icons">toc</i> <?php echo strtoupper($row->JUDUL); ?></small></h5><hr/>
					<div id='cont' style='font-family:callibri'><?php 
						echo  word_limiter(strip_tags($row->DESKRIPSI,20)); 
					?></div>
					<br/>
					<br/>
					<a href='<?php echo site_url('home/detail/'.$row->URL.'.html')?>' class="btn btn-small blue waves-effect waves-light z-depth-0" name="action">Read More
						<i class="material-icons right">send</i>
					</a>
					<br/>
					<p class='right' style='color:grey'> Tanggal : <?php echo tgl_indo($row->TGL_INPUT) ?> | <?php echo $row->NAMA_PENGIRIM ?> | Kategori : <?php echo $kat;?></p>
				</div>
			<?php } $nom++;
			}
		} ?>
		<?php echo $paging['list'] ?>		

	<!--img class="materialboxed" data-caption="A picture of some deer and tons of trees" width="250" src="<?php echo base_url($row->IMAGE)?>"!-->
					
</div>


<script>
$(document).ready(function(){
    $('.materialboxed').materialbox();
  });
      
$(function () {
	$('[data-toggle=\"tooltip\"]').tooltip()
})
	
</script>