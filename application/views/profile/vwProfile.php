<style>
table tr td{text-valign:top}
</style>

<?php 
	
	$lvl = $this->session->userdata('level');
	$jk	 = $this->session->userdata('jenis_kelamin');
	
	if($jk==1) $jk = 'Laki - Laki';
		else $jk = 'Perepmpuan';
		
	if($lvl==2) $stts = 'Guru';
		else $stts = 'Siswa';
		
		
	if($lvl==2) {
		$list = $this->m_guru->getProfile($this->session->userdata('id'));
	} else {
		$list = $this->m_siswa->getProfile($this->session->userdata('id'));
	}
?>
<div class='row'>
	<div class='col-md-3 col-sm-12'>
		<img src='<?php echo $list['IMAGE']?>' class='img-responsive' />
		
		<br/><a href='<?php echo $lvl == 2 ? site_url('guru/input/'.encode($this->session->userdata('id'))) : site_url('siswa/input/'.encode($this->session->userdata('id')))?>' class='btn btn-primary'><i class='fa fa-user'></i> Ubah Profile</a>
	</div>
	<div class='col-md-9 col-sm-12'>
		<table class='table-responsive'>
			<tr height='30px'>
				<td valign='top'><?php echo $lvl==2 ? 'NIP' : 'NIS' ?> </td><td valign='top'> : </td><td valign='top'><strong> <?php if($lvl==2) echo $list['NIP']; else echo $list['NIS']?></strong></td>
			</tr>
			<tr height='30px'>
				<td valign='top' style='width:200px'>Nama Lengkap</td><td valign='top' style='width:20px'> : </td><td valign='top'> <?php echo $list['NAMA']?></td>
			</tr>
			<tr height='30px'>
				<td valign='top'>Jenis Kelamin</td><td valign='top'> : </td><td valign='top'> <?php echo $jk ?></td>
			</tr>
			<tr height='30px'>
				<td valign='top'>Tanggal Lahir</td><td valign='top'> : </td><td valign='top'> <?php echo tgl_indo($list['TGL_LAHIR'])?></td>
			</tr>
			<tr height='30px'>
				<td valign='top'>No Hp</td><td valign='top'> : </td><td valign='top'> <?php echo $list['NO_HP']?></td>
			</tr>
			<tr height='30px'>
				<td valign='top'>Email</td><td valign='top'> : </td><td valign='top'> <?php echo $list['EMAIL']?></td>
			</tr>
			<tr height='30px'>
				<td valign='top'>Alamat</td><td valign='top'> : </td><td valign='top'> <?php echo $list['ALAMAT']?></td>
			</tr>
			<tr height='30px'>
				<td valign='top'>Status</td><td valign='top'> : </td><td valign='top'> <?php echo $list['STATUS']?></td>
			</tr>
		</table>
	</div>
</div>