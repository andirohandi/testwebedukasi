<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			if(!$this->session->userdata('logedIn')) {
				redirect('adm/login','refresh');
			}
			
			$this->load->library('form_validation');
			$this->load->helper('MY_date_helper');
			$this->load->helper(array('form'));
			$this->load->model('m_forum');
			$this->load->model('m_guru');
			$this->load->model('m_siswa');
	}
	
	
	public function index()
	{
		$data['title'] 		= "Forum";
		$data['titlepage'] 	= "Data forum";
		$data['view'] 		= "forum/vwforum";
		$this->load->view('vwIndex',$data);
	}
	
	public function input($id='',$msg='')
	{
		
		if(decode($id)!=0) {
			$id = decode($id);
			$title = "Ubah Forum";
			$data['forum'] = $this->m_forum->getDetail($id);
		}	
		else {
			$id = 0;
			$title = "Input Forum";
		}
		
		
		
		
		$data['title'] 		= $title;
		$data['titlepage'] 	= $title;
		$data['view'] 		= "forum/vwInput";
		$data['msg'] 		= $msg;
		$this->load->view('vwIndex',$data);
	}
	
	public function create(){

		$ID 		= decode($_POST['id_forum']);
		$JUDUL 		= trim(mysql_real_escape_string($_POST['judul']));
		$PENGIRIM	= $this->session->userdata('id');
		$DESKRIPSI 	= $_POST['deskripsi'];
		$TGL_INPUT 	= date('Y-m-d');//trim(mysql_real_escape_string($_POST['tanggal_lahir']));
		$STATUS		= trim(mysql_real_escape_string($_POST['status']));
		$psn 		= '';
		$url_judul  = trim(preg_replace('^[\\\\/:\*\?!.,;\"<>\|]^',' ',$JUDUL));
		$URL		= strtolower(str_replace(' ','-',$url_judul));
		
		$data = array(
			'ID' 		=> '',
			'JUDUL' 	=> $JUDUL,
			'PENGIRIM' 	=> $this->session->userdata('id'),
			'DESKRIPSI'	=> $DESKRIPSI,
			'TGL_INPUT' => $TGL_INPUT,
			'URL' 		=> $URL,
			'STATUS' 	=> $STATUS
		);
		
		$data2 = array(
			'JUDUL' 	=> $JUDUL,
			'DESKRIPSI'	=> $DESKRIPSI,
			'URL' 		=> $URL,
			'STATUS' 	=> $STATUS
		);

		$this->db->trans_begin();
		
		if($_POST['id_forum'] != '')
		{
		
			$this->m_forum->getUpdate($data2, $ID);
			$url = $_POST['id_forum'];
			$psn = "Diubah";
		}
		else
		{
			$this->m_forum->getInsert($data);		
			$url = encode(0);
			$psn = "Disimpan";
		}
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$msg = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i> Data Gagal Disimpan</h4></div>');
			redirect(base_url().'forum/input/'.$url.'/'.$msg);
		} else {
			$this->db->trans_commit();
			$msg = encode('<div class="alert alert-success" role="alert"><h4><i class="fa fa-check"></i> Data Berhasil '.$psn.'</h4></div>');
			redirect(base_url().'forum/input/'.$url.'/'.$msg);
		}
		
	}
	
	
	function read($pg=1) {		
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;
		$id_user	= $this->session->userdata('id');
		if($this->session->userdata('level')==1) {
			$like = "(b.NAMA like '%$key_text%' OR b.NIP like '%$key_text%' OR a.JUDUL like '%$key_text%' OR a.DESKRIPSI like '%$key_text%' )";
		} else {
			$like = "((b.NAMA like '%$key_text%' OR b.NIP like '%$key_text%' OR a.JUDUL like '%$key_text%' OR a.DESKRIPSI like '%$key_text%') AND a.PENGIRIM='$id_user')";
		}
		
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_forum->count($like);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_forum->getSelect($like, $limit, $offset);
		
		$this->load->view('forum/vwList', $data);
	}
	
	function delete() {
		$id = decode($_POST['i']);
		
		$this->db->trans_begin();
		
		
		
		$this->db->delete('tbl_forum', array('ID' => $id));
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$rs = 0;
		} else {
			$this->db->trans_commit();
			$rs = 1;
		}
		
		$dt = array(
			'rs'=>$rs
		);
		echo json_encode($dt);
	}
}
	