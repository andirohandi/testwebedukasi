<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			if(!$this->session->userdata('logedIn')) {
				redirect('adm/login','refresh');
		}
		
		$this->load->library('form_validation');
		$this->load->helper('MY_date_helper');
		$this->load->helper(array('form'));
		$this->load->model('m_front');
		$this->load->model('m_guru');
		$this->load->model('m_siswa');
	}
	
	function index($pg='',$msg='') {
		$data['msg'] 		= $msg;
		$data['title'] 		= "Setting";
		$data['titlepage'] 	= "Pengaturan";
		$data['view'] 		= "front/vwFront";
		$data['list']		= $this->m_front->getSelect();
		
		$this->load->view('vwIndex',$data);
	}
	
	function updateHome() {
		$this->load->helper('file');
		
		$config['upload_path'] = './uploads/images/';
		$config['allowed_types'] =  'gif|jpg|png';
		$config['max_size'] = '5120';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$THUMBNAIL = '';
		$IMAGE = '';
		
		if($this->upload->do_upload('upload-image'))
		{
			$file = $this->upload->data();
			
			$IMAGE = 'uploads/images/'.$file['file_name'];
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $file['full_path'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 100;
			$config['height'] = 100;
			$config['new_image'] = './uploads/thumbnails/';

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			$THUMBNAIL = 'uploads/thumbnails/'.$file['raw_name'].'_thumb'.$file['file_ext'];
			
			$this->image_lib->clear();
		} else {
			$type = get_mime_by_extension($_FILES['upload-image']['name']);
			
			if(($type != 'image/jpeg' || $type != 'image/png' || $type != 'image/gif') && $_FILES['upload-image']['size'] > $config['max_size']) {
				$error = $this->upload->display_errors();
				$error = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i>' . $error . '</h4></div>');
				redirect(base_url().'front/index/'.encode(0).'/'.$error);
			}
		}
		
		$data = array (
			'IMAGE_HOME' => $IMAGE
		);
		
		$this->db->trans_begin();
		$this->m_front->getUpdate($data);
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$msg = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i> Data Gagal Disimpan</h4></div>');
			redirect(base_url().'front/index/'.$url.'/'.$msg);
		} else {
			$this->db->trans_commit();
			$msg = encode('<div class="alert alert-success" role="alert"><h4><i class="fa fa-check"></i> Data Berhasil '.$psn.'</h4></div>');
			redirect(base_url().'front/index/'.$url.'/'.$msg);
		}
		
	}
	
	function updateAbout() {
		$about = $_POST['about'];
		$data = array(
			'ABOUT' => $about	
		);
		
		$dt = array();
		
		$this->db->trans_begin();
		$this->m_front->getUpdate($data);
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$dt = array (
				'rs' =>0
			);
		} else {
			$this->db->trans_commit();
			$dt = array (
				'rs' =>1
			);
		}
		
		echo json_encode($dt);
	} 
	
	function updateVisi() {
		$visi = $_POST['visi'];
		$data = array(
			'VISI_MISI' => $visi	
		);
		
		$dt = array();
		
		$this->db->trans_begin();
		$this->m_front->getUpdate($data);
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$dt = array (
				'rs' =>0
			);
		} else {
			$this->db->trans_commit();
			$dt = array (
				'rs' =>1
			);
		}
		
		echo json_encode($dt);
	}
	
}