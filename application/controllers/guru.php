<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Guru extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			if(!$this->session->userdata('logedIn')) {
				redirect('adm/login','refresh');
			}
			
			$this->load->library('form_validation');
			$this->load->helper('MY_date_helper');
			$this->load->helper(array('form'));
			$this->load->model('m_guru');
			$this->load->model('m_siswa');
			$this->load->model('m_jabatan');
	}
	
	
	public function index()
	{
		$data['title'] 		= "Dashboard";
		$data['titlepage'] 	= "Data Guru";
		$data['view'] 		= "guru/vwGuru";
		$this->load->view('vwIndex',$data);
	}
	
	public function input($id='',$msg='')
	{
		
		if(decode($id)!=0) {
			$id = decode($id);
			$title = "Ubah Data Guru";
		}	
		else {
			$id = 0;
			$title = "Input Data Guru";
		}
		
		$data['jbtn']	= $this->m_jabatan->getSelectOption();
		
		$data['guru'] = $this->m_guru->getDetail(array('ID' => $id));
		
		$data['title'] 		= $title;
		$data['titlepage'] 	= $title;
		$data['view'] 		= "guru/vwInput";
		$data['msg'] 		= $msg;
		$this->load->view('vwIndex',$data);
	}
	
	public function create(){
		$this->load->helper('file');
		$id 	= decode($_POST['id_guru']);
		$NIP 	= trim(mysql_real_escape_string($_POST['nip']));
		$NAMA	= trim(mysql_real_escape_string($_POST['nama_guru']));
		$JABATAN= trim(mysql_real_escape_string($_POST['jabatan']));
		$ALAMAT = $_POST['alamat'];
		$JK 	= trim(mysql_real_escape_string($_POST['jenis_kelamin']));
		$TGL_LAHIR 	= date('Y-m-d');//trim(mysql_real_escape_string($_POST['tanggal_lahir']));
		$NO_HP 	= trim(mysql_real_escape_string($_POST['no_hp']));
		$EMAIL 	= trim(mysql_real_escape_string($_POST['email']));
		$LEVEL	= '2';
		$STATUS	= trim(mysql_real_escape_string($_POST['status']));
		$psn 	= '';
		
		
		$config['upload_path'] = './uploads/images/';
		$config['allowed_types'] =  'gif|jpg|png';
		$config['max_size'] = '5120';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$THUMBNAIL = '';
		$IMAGE = '';
		
		if($this->upload->do_upload('upload-image'))
		{
			$file = $this->upload->data();
			
			$IMAGE = 'uploads/images/'.$file['file_name'];
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $file['full_path'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 100;
			$config['height'] = 100;
			$config['new_image'] = './uploads/thumbnails/';

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			$THUMBNAIL = 'uploads/thumbnails/'.$file['raw_name'].'_thumb'.$file['file_ext'];
			
			$this->image_lib->clear();
		} else {
			$type = get_mime_by_extension($_FILES['upload-image']['name']);
			
			if(($type != 'image/jpeg' || $type != 'image/png' || $type != 'image/gif') && $_FILES['upload-image']['size'] > $config['max_size']) {
				$error = $this->upload->display_errors();
				$error = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i>' . $error . '</h4></div>');
				redirect(base_url().'guru/input/'.encode(0).'/'.$error);
			}
		}
		
		if($IMAGE) {
			$img = $IMAGE; 
			$thum = $THUMBNAIL;
		} else { 
			$img = 'uploads/img/no_image.png'; 
			$thum = 'uploads/img/no_image.png';
		}
		
		$data = array(
			'ID' 		=> '',
			'NIP' 		=> $NIP,
			'NAMA' 		=> $NAMA,
			'JABATAN'	=> $JABATAN,
			'ALAMAT		'=> $ALAMAT,
			'JENIS_KELAMIN' => $JK,
			'TGL_LAHIR' => $TGL_LAHIR,
			'NO_HP' 	=> $NO_HP,
			'EMAIL' 	=> $EMAIL,
			'LEVEL' 	=> $LEVEL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $img,
			'THUMBNAIL' => $thum
		);
		
		$data2 = array(
			'NIP' 		=> $NIP,
			'NAMA' 		=> $NAMA,
			'JABATAN'	=> $JABATAN,
			'ALAMAT		'=> $ALAMAT,
			'JENIS_KELAMIN' => $JK,
			'TGL_LAHIR' => $TGL_LAHIR,
			'NO_HP' 	=> $NO_HP,
			'EMAIL' 	=> $EMAIL,
			'LEVEL' 	=> $LEVEL,
			'STATUS' 	=> $STATUS
		);
		
		$data3 = array(
			'NIP' 		=> $NIP,
			'NAMA' 		=> $NAMA,
			'JABATAN'	=> $JABATAN,
			'ALAMAT		'=> $ALAMAT,
			'JENIS_KELAMIN' => $JK,
			'TGL_LAHIR' => $TGL_LAHIR,
			'NO_HP' 	=> $NO_HP,
			'EMAIL' 	=> $EMAIL,
			'LEVEL' 	=> $LEVEL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $img,
			'THUMBNAIL' => $thum
		);
		
		
		$this->db->trans_begin();
		
		
		if($id!=0)
		{
			if($IMAGE) {
				$this->m_guru->getUpdate($data3, $id);
			}else {
				$this->m_guru->getUpdate($data2, $id);
			}
			
			$url = $_POST['id_guru'];
			$psn = "Diubah";
		}
		else
		{
			$this->m_guru->getInsert($data);
			$this->db->query("INSERT INTO tbl_user(ID,USERNAME,PASSWORD,LEVEL) values('','".$NIP."','".md5($NIP)."',2)");			
			$url = encode(0);
			$psn = "Disimpan";
		}
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$msg = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i> Data Gagal Disimpan</h4></div>');
			redirect(base_url().'guru/input/'.$url.'/'.$msg);
		} else {
			$this->db->trans_commit();
			$msg = encode('<div class="alert alert-success" role="alert"><h4><i class="fa fa-check"></i> Data Berhasil '.$psn.'</h4></div>');
			redirect(base_url().'guru/input/'.$url.'/'.$msg);
		}
		
	}
	
	function read($pg=1) {		
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;
		
		$like = "(NAMA like '%$key_text%' OR NIP like '%$key_text%')";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_guru->count($like);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_guru->getSelect($like, $limit, $offset);
		
		$this->load->view('guru/vwList', $data);
	}
	
	function update() {
		
	}
	
	function delete() {
		$id = decode($_POST['i']);
		$image = $_POST['x'];
		
		$this->db->trans_begin();
		
		$pos = $this->m_guru->get(array('ID' => $id));
		
		if($image!="uploads/img/no_image.png") {
			//hapus file foto
			unlink(FCPATH.$pos['IMAGE']);
			unlink(FCPATH.$pos['THUMBNAIL']);
		}
		$this->db->delete('tbl_guru', array('ID' => $id));
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$rs = 0;
		} else {
			$this->db->trans_commit();
			$rs = 1;
		}
		
		$dt = array(
			'rs'=>$rs
		);
		echo json_encode($dt);
	}
	
}