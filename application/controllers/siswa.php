<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siswa extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			if(!$this->session->userdata('logedIn')) {
				redirect('adm/login','refresh');
			}
			
			$this->load->library('form_validation');
			$this->load->helper('MY_date_helper');
			$this->load->helper(array('form'));
			$this->load->model('m_siswa');
			$this->load->model('m_guru');
	}
	
	function  index() {
		$data['title'] 		= "Data Siswa";
		$data['titlepage'] 	= "Data Siswa";
		$data['view'] 		= "siswa/vwSiswa";
		$this->load->view('vwIndex',$data);
	}
	
	public function input($id='',$msg='')
	{
		
		if(encode($id)!='') {
			$id = decode($id);
			$title = "Ubah Data Siswa";
		}	
		else {
			$id = '';
			$title = "Input Data Siswa";
		}
		
		$data['siswa'] = $this->m_siswa->getDetail(array('ID' => $id));
		
		$data['title'] 		= $title;
		$data['titlepage'] 	= $title;
		$data['view'] 		= "siswa/vwInput";
		$data['msg'] 		= $msg;
		$this->load->view('vwIndex',$data);
	}
	
	public function create(){
		$this->load->helper('file');
		$ID 	= decode($_POST['id_siswa']);
		$NIS 	= trim(mysql_real_escape_string($_POST['nis']));
		$NAMA	= trim(mysql_real_escape_string($_POST['nama_siswa']));
		$ALAMAT = $_POST['alamat'];
		$JK 	= trim(mysql_real_escape_string($_POST['jenis_kelamin']));
		$TGL_LAHIR 	= date('Y-m-d');//trim(mysql_real_escape_string($_POST['tanggal_lahir']));
		$NO_HP 	= trim(mysql_real_escape_string($_POST['no_hp']));
		$EMAIL 	= trim(mysql_real_escape_string($_POST['email']));
		$LEVEL	= '3';
		$STATUS	= trim(mysql_real_escape_string($_POST['status']));
		$psn 	= '';
		
		
		$config['upload_path'] = './uploads/images/';
		$config['allowed_types'] =  'gif|jpg|png';
		$config['max_size'] = '5120';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$THUMBNAIL = '';
		$IMAGE = '';
		
		if($this->upload->do_upload('upload-image'))
		{
			$file = $this->upload->data();
			
			$IMAGE = 'uploads/images/'.$file['file_name'];
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $file['full_path'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 100;
			$config['height'] = 100;
			$config['new_image'] = './uploads/thumbnails/';

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			$THUMBNAIL = 'uploads/thumbnails/'.$file['raw_name'].'_thumb'.$file['file_ext'];
			
			$this->image_lib->clear();
		} else {
			$type = get_mime_by_extension($_FILES['upload-image']['name']);
			
			if(($type != 'image/jpeg' || $type != 'image/png' || $type != 'image/gif') && $_FILES['upload-image']['size'] > $config['max_size']) {
				$error = $this->upload->display_errors();
				$error = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i>' . $error . '</h4></div>');
				redirect(base_url().'guru/input/'.encode(0).'/'.$error);
			}
		}
		
		if($IMAGE) {
			$img = $IMAGE; 
			$thum = $THUMBNAIL;
		} else { 
			$img = 'uploads/img/no_image.png'; 
			$thum = 'uploads/img/no_image.png';
		}
		
		
		$data = array(
			'ID' 		=> '',
			'NIS' 		=> $NIS,
			'NAMA' 		=> $NAMA,
			'ALAMAT		'=> $ALAMAT,
			'JENIS_KELAMIN' => $JK,
			'TGL_LAHIR' => $TGL_LAHIR,
			'NO_HP' 	=> $NO_HP,
			'EMAIL' 	=> $EMAIL,
			'LEVEL' 	=> $LEVEL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $img,
			'THUMBNAIL' => $thum
		);
		
		$data2 = array(
			'NIS' 		=> $NIS,
			'NAMA' 		=> $NAMA,
			'ALAMAT		'=> $ALAMAT,
			'JENIS_KELAMIN' => $JK,
			'TGL_LAHIR' => $TGL_LAHIR,
			'NO_HP' 	=> $NO_HP,
			'EMAIL' 	=> $EMAIL,
			'LEVEL' 	=> $LEVEL,
			'STATUS' 	=> $STATUS
		);
		
		$data3 = array(
			'NIS' 		=> $NIS,
			'NAMA' 		=> $NAMA,
			'ALAMAT		'=> $ALAMAT,
			'JENIS_KELAMIN' => $JK,
			'TGL_LAHIR' => $TGL_LAHIR,
			'NO_HP' 	=> $NO_HP,
			'EMAIL' 	=> $EMAIL,
			'LEVEL' 	=> $LEVEL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $img,
			'THUMBNAIL' => $thum
		);
		
		$this->db->trans_begin();
		
		
		if($_POST['id_siswa'])
		{
			if($IMAGE) {
				$this->m_siswa->getUpdate($data3, $ID);
			}else {
				$this->m_siswa->getUpdate($data2, $ID);
			}
			
			$url = $_POST['id_siswa'];
			$psn = "Diubah";
		}
		else
		{
			
			$this->m_siswa->getInsert($data);
			$this->db->query("INSERT INTO tbl_user(ID,USERNAME,PASSWORD,LEVEL) values('','".$NIS."','".md5($NIS)."',3)");
			$url = encode(0);
			$psn = "Disimpan";
		}
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$msg = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i> Data Gagal Disimpan</h4></div>');
			redirect(base_url().'siswa/input/'.$url.'/'.$msg);
		} else {
			$this->db->trans_commit();
			$msg = encode('<div class="alert alert-success" role="alert"><h4><i class="fa fa-check"></i> Data Berhasil '.$psn.'</h4></div>');
			redirect(base_url().'siswa/input/'.$url.'/'.$msg);
		}
		
	}
	
	function read($pg=1) {		
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;
		
		$like = "(NAMA like '%$key_text%' OR NIS like '%$key_text%')";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_siswa->count($like);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_siswa->getSelect($like, $limit, $offset);
		
		$this->load->view('siswa/vwList', $data);
	}
	
	function update() {
		
	}
	
	function delete() {
		$id = decode($_POST['i']);
		$image = $_POST['x'];
		
		$this->db->trans_begin();
		
		$pos = $this->m_siswa->get(array('ID' => $id));
		
		if($image!="uploads/img/no_image.png") {
			//hapus file foto
			unlink(FCPATH.$pos['IMAGE']);
			unlink(FCPATH.$pos['THUMBNAIL']);
		}
		$this->db->delete('tbl_siswa', array('ID' => $id));
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$rs = 0;
		} else {
			$this->db->trans_commit();
			$rs = 1;
		}
		
		$dt = array(
			'rs'=>$rs
		);
		echo json_encode($dt);
	}
	
	
	//pencapaian seksion
	
	function pencapaian() {
		$data['title'] 		= "Siswa";
		$data['titlepage'] 	= "Pencapaian";
		$data['view'] 		= "siswa/vwPencapaian";
		$this->load->view('vwIndex',$data);
	}
	
	function readPencapaian() {
		
		
		$data['list'] 		= $this->m_siswa->getSelectPencapaian($this->session->userdata('id'));
		
		$this->load->view('siswa/vwListPencapaian', $data);
	}
	
	function createPencapaian() {
		$pencapaian = mysql_real_escape_string(trim($_POST['pencapaian']));
		$tahun = mysql_real_escape_string(trim($_POST['tahun']));
		$id_siswa = $this->session->userdata('id');
		
		$data = array('TAHUN'=>$tahun,'PENCAPAIAN'=>$pencapaian,'ID_SISWA'=> $id_siswa);
		$dt   = array();
		
		if($tahun != '' && $pencapaian!='') {
			
			$cek = $this->m_siswa->cekPencapaian($id_siswa,$tahun);
			
			if($cek->num_rows>0) {
				$dt = array('rs'=>2);
			} else {
				$this->m_siswa->getInsertPencapaian($data);
				$dt = array('rs'=>1);
			}
		} else {
			$dt = array('rs'=>0);
		}
		
		echo json_encode($dt);
	}
	
	
	function updatePencapaian() {
		$id = trim($_POST['id']);
		$pencapaian = mysql_real_escape_string(trim($_POST['pencapaian']));
		$tahun = mysql_real_escape_string(trim($_POST['tahun']));
		
		$data = array('TAHUN'=>$tahun, 'PENCAPAIAN'=>$pencapaian);
		
		$this->db->update('tbl_pencapaian',$data,array('ID'=>$id));
		
		$dt = array('rs'=>1);
		
		echo json_encode($dt);
		
		
	}
	
	function deletePencapaian() {
		$id = decode($_POST['i']);
		
		$this->db->delete('tbl_pencapaian', array('ID' => $id, 'ID_SISWA' => $this->session->userdata('id')));
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$rs = 0;
		} else {
			$this->db->trans_commit();
			$rs = 1;
		}
		
		$dt = array(
			'rs'=>$rs
		);
		echo json_encode($dt);
	}
	
	// log history
	
	function history() {
		$data['title'] 		= "Siswa";
		$data['titlepage'] 	= "History";
		$data['view'] 		= "siswa/vwHistory";
		$this->load->view('vwIndex',$data);
	}
	
	function readHistory() {
		
		
		$data['list'] 		= $this->m_siswa->getHistoryComent($this->session->userdata('username'));
		
		$this->load->view('siswa/vwListHistory', $data);
	}
}
	