<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adm extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			$this->load->library('form_validation');
			$this->load->helper('MY_date_helper');
			$this->load->helper(array('form'));
			$this->load->model('m_adm');
			$this->load->model('m_post');
			$this->load->model('m_guru');
			$this->load->model('m_siswa');
	}
	
	
	public function index()
	{
		if(!$this->session->userdata('logedIn')) {
			redirect('adm/login','refresh');
		}
		
		redirect('adm/dashboard');
	}
	
	public function dashboard() {
		if(!$this->session->userdata('logedIn')) {
			redirect('adm/login','refresh');
		}
		$where = '';
		$data['title'] 		= "Dashboard";
		$data['titlepage'] 	= "Dashboard";
		$data['view'] 		= "vwDashboard";
		$data['guru']		= $this->m_guru->count();
		$data['siswa']		= $this->m_siswa->count();
		$data['article']	= $this->m_post->count($where,2);
		$data['inform']		= $this->m_post->count($where,1);
		$data['galeri']		= $this->m_post->count($where,3);
		
		
		$this->load->view('vwIndex',$data);
	}
	
	public function login() {
		
		$data['title'] 		= "Login Page";
		$this->load->view('vwLogin',$data);
	}
	
	public function doLogin()
	{
		$username = trim(mysql_real_escape_string($this->input->post('username')));
		$password = md5(trim(mysql_real_escape_string($this->input->post('password'))));
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
		
		if($this->form_validation->run() == FALSE)
		{
	     
		 $data['title'] = "Login Page";
	     $this->load->view('vwLogin',$data);
		}
		else
		{
	     redirect('adm', 'refresh');
		}
		
		
	}
	
	public function doLoginUser()
	{
		$username = trim(mysql_real_escape_string($this->input->post('username')));
		$password = md5(trim(mysql_real_escape_string($this->input->post('password'))));
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
		
		if($this->form_validation->run() == FALSE)
		{
	     
			redirect('');
		}
		else
		{
	     redirect('adm', 'refresh');
		}
		
		
	}
	
	function check_database()
	{
		
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
	 
	   
		$data = array(
			'USERNAME' => $username,
			'PASSWORD' => $password
		);
		
		$level = '';
		$sess_array = array();
		
		$result = $this->m_adm->cekLogin($data);
	 
		if($result)
		{
			foreach($result as $r) {
				$level = $r->LEVEL;
			}
			
			if($level==1) {
				foreach($result as $row)
				{
					$sess_array = array(
						'id' => $row->ID,
						'username' => $row->USERNAME,
						'nama' => $row->USERNAME,
						'level' => $row->LEVEL,
						'logedIn' => true
					);
					
					$this->session->set_userdata($sess_array);
				}
			}else if($level==2) {
				$resultGuru = $this->m_adm->cekLoginGuru($username);
				
				if($resultGuru) {
					foreach($resultGuru as $rowGuru)
					{
						$sess_array = array(
							'id' 		=> $rowGuru->ID,
							'username' 	=> $rowGuru->NIP,
							'nama' 		=> $rowGuru->NAMA,
							'jenis_kelamin' => $rowGuru->JENIS_KELAMIN,
							'tgl_lahir' => $rowGuru->TGL_LAHIR,
							'no_hp' 	=> $rowGuru->NO_HP,
							'email' 	=> $rowGuru->EMAIL,
							'image' 	=> $rowGuru->IMAGE,
							'thumbnail' => $rowGuru->THUMBNAIL,
							'alamat' 	=> $rowGuru->ALAMAT,
							'level' 	=> $rowGuru->LEVEL,
							'logedIn' 	=> true
						);
						
						$this->session->set_userdata($sess_array);
					}
				}
			}else if($level==3) {
				$resultGuru = $this->m_adm->cekLoginSiswa($username);
				
				if($resultGuru) {
					foreach($resultGuru as $rowGuru)
					{
						$sess_array = array(
							'id' 		=> $rowGuru->ID,
							'username' 	=> $rowGuru->NIS,
							'nama' 		=> $rowGuru->NAMA,
							'jenis_kelamin' => $rowGuru->JENIS_KELAMIN,
							'alamat' 	=> $rowGuru->ALAMAT,
							'tgl_lahir' => $rowGuru->TGL_LAHIR,
							'no_hp' 	=> $rowGuru->NO_HP,
							'email' 	=> $rowGuru->EMAIL,
							'image' 	=> $rowGuru->IMAGE,
							'thumbnail' => $rowGuru->THUMBNAIL,
							'level' 	=> $rowGuru->LEVEL,
							'logedIn' 	=> true
						);
						
						$this->session->set_userdata($sess_array);
					}
				}
			}
			
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}
	
	function doLogout()
	{
		$this->session->unset_userdata('logedIn');
		session_destroy();
		redirect('');
	}
	function doLogoutUser()
	{
		$this->session->unset_userdata('logedIn');
		session_destroy();
		redirect('');
	}
	
}