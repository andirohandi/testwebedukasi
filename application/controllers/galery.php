<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galery extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			if(!$this->session->userdata('logedIn')) {
				redirect('adm/login','refresh');
		}
		
		$this->load->library('form_validation');
		$this->load->helper('MY_date_helper');
		$this->load->helper(array('form'));
		$this->load->model('m_post');
		$this->load->model('m_guru');
		$this->load->model('m_siswa');
	}
	
	public function index()
	{
		$data['title'] 		= "Galeri";
		$data['titlepage'] 	= "Album Galeri";
		$data['view'] 		= "galeri/vwGaleri";
		$this->load->view('vwIndex',$data);
	}

	public function input($id='',$msg='')
	{
		
		if(decode($id)!=0) {
			$id = decode($id);
			$title = "Ubah Image";
		}	
		else {
			$id = '';
			$title = "Input Image";
		}
		
		$data['post'] = $this->m_post->getDetail(array('a.ID' => $id));
		
		$data['title'] 		= $title;
		$data['titlepage'] 	= $title;
		$data['view'] 		= "galeri/vwInput";
		$data['msg'] 		= $msg;
		$this->load->view('vwIndex',$data);
	}
	
	public function create(){
		$this->load->helper('file');
		$ID 		= decode($_POST['id_post']);
		$JUDUL 		= trim(mysql_real_escape_string($_POST['judul']));
		$KATEGORI 	= 3;
		$DESKRIPSI 	= $_POST['deskripsi'];
		$TGL_INPUT 	= date('Y-m-d');
		$STATUS		= trim(mysql_real_escape_string($_POST['status']));
		$psn 		= '';
		$URL		= strtolower(str_replace(' ','-',$JUDUL));
		
		
		$config['upload_path'] = './uploads/images/';
		$config['allowed_types'] =  'gif|jpg|png';
		$config['max_size'] = '5120';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		$THUMBNAIL = '';
		$IMAGE = '';
		
		if($this->upload->do_upload('upload-image'))
		{
			$file = $this->upload->data();
			
			$IMAGE = 'uploads/images/'.$file['file_name'];
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $file['full_path'];
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 100;
			$config['height'] = 100;
			$config['new_image'] = './uploads/thumbnails/';

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			$THUMBNAIL = 'uploads/thumbnails/'.$file['raw_name'].'_thumb'.$file['file_ext'];
			
			$this->image_lib->clear();
		} else {
			$type = get_mime_by_extension($_FILES['upload-image']['name']);
			
			if(($type != 'image/jpeg' || $type != 'image/png' || $type != 'image/gif') && $_FILES['upload-image']['size'] > $config['max_size']) {
				$error = $this->upload->display_errors();
				$error = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i>' . $error . '</h4></div>');
				redirect(base_url().'galery/input/'.encode(0).'/'.$error);
			}
		}
		
		if($IMAGE) {
			$img = $IMAGE; 
			$thum = $THUMBNAIL;
		} else { 
			$img = 'uploads/img/no_image.png'; 
			$thum = 'uploads/img/no_image.png';
		}
		
		$data = array(
			'ID' 		=> '',
			'JUDUL' 	=> $JUDUL,
			'PENGIRIM' 	=> $this->session->userdata('id'),
			'KATEGORI'	=> $KATEGORI,
			'DESKRIPSI'	=> $DESKRIPSI,
			'TGL_INPUT' => $TGL_INPUT,
			'URL' 		=> $URL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $img,
			'THUMBNAIL' => $thum
		);
		
		$data2 = array(
			'JUDUL' 	=> $JUDUL,
			'PENGIRIM' 	=> $this->session->userdata('id'),
			'KATEGORI'	=> $KATEGORI,
			'DESKRIPSI'	=> $DESKRIPSI,
			'URL' 		=> $URL,
			'STATUS' 	=> $STATUS
		);
		
		
		$data3 = array(
			'JUDUL' 	=> $JUDUL,
			'PENGIRIM' 	=> $this->session->userdata('id'),
			'KATEGORI'	=> $KATEGORI,
			'DESKRIPSI'	=> $DESKRIPSI,
			'URL' 		=> $URL,
			'STATUS' 	=> $STATUS,
			'IMAGE' 	=> $img,
			'THUMBNAIL' => $thum
		);
		
		$this->db->trans_begin();
		
		if($_POST['id_post'] != '')
		{
			if($IMAGE) {
				$this->m_post->getUpdate($data3, $ID);
			}else {
				$this->m_post->getUpdate($data2, $ID);
			}
			
			$url = $_POST['id_post'];
			$psn = "Diubah";
		}
		else
		{
			$this->m_post->getInsert($data);		
			$url = encode(0);
			$psn = "Disimpan";
		}
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$msg = encode('<div class="alert alert-danger" role="alert"><h4><i class="glyphicon glyphicon-remove"></i> Data Gagal Disimpan</h4></div>');
			redirect(base_url().'galery/input/'.$url.'/'.$msg);
		} else {
			$this->db->trans_commit();
			$msg = encode('<div class="alert alert-success" role="alert"><h4><i class="fa fa-check"></i> Data Berhasil '.$psn.'</h4></div>');
			redirect(base_url().'galery/input/'.$url.'/'.$msg);
		}
		
	}
	
	function read($pg=1) {		
		$kategori	= 3;
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;
		$level		= $this->session->userdata('level');
		$id_p		= $this->session->userdata('id');
		
		if($level==1)
			$like = "((a.JUDUL like '%$key_text%' OR b.NAMA like '%$key_text%' OR b.NIP like '%$key_text%') AND KATEGORI= '$kategori')";
		else 
			$like = "((a.JUDUL like '%$key_text%' OR b.NAMA like '%$key_text%' OR b.NIP like '%$key_text%') AND a.PENGIRIM='$id_p')";
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_post->count($like,$kategori);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_post->getSelect($like, $limit, $offset, $kategori);
		
		$this->load->view('galeri/vwList', $data);
	}
	
	function delete() {
		$id = decode($_POST['i']);
		$image = $_POST['x'];
		
		$this->db->trans_begin();
		
		$pos = $this->m_post->get(array('ID' => $id));
		
		if($image!="uploads/img/no_image.png") {
			//hapus file foto
			unlink(FCPATH.$pos['IMAGE']);
			unlink(FCPATH.$pos['THUMBNAIL']);
		}
		$this->db->delete('tbl_post', array('ID' => $id));
		
		if($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			$rs = 0;
		} else {
			$this->db->trans_commit();
			$rs = 1;
		}
		
		$dt = array(
			'rs'=>$rs
		);
		echo json_encode($dt);
	}
}