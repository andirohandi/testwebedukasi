<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			if(!$this->session->userdata('logedIn')) {
				redirect('adm/login','refresh');
			}
			
			$this->load->library('form_validation');
			$this->load->helper('MY_date_helper');
			$this->load->helper(array('form'));
			$this->load->model('m_jabatan');
			$this->load->model('m_guru');
			$this->load->model('m_siswa');
	}
	
	public function index()
	{
		$data['title'] 		= "Jabatan";
		$data['titlepage'] 	= "Data Jabatan";
		$data['view'] 		= "jabatan/vwJabatan";
		$this->load->view('vwIndex',$data);
	}
	
	function read($pg=1) {		
		$limit 		= $_POST['limit'];
		$key_text 	= $_POST['key'];
		$offset 	= ($limit*$pg)-$limit;
		
		$like = "(JABATAN like '%$key_text%')";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_jabatan->count($like);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_paging($page);
		
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_jabatan->getSelect($like, $limit, $offset);
		
		$this->load->view('jabatan/vwList', $data);
	}
	
	function create() {
		$nama = mysql_real_escape_string(trim($_POST['nama_jabatan']));
		$status = mysql_real_escape_string(trim($_POST['status']));
		
		$data = array('JABATAN'=>$nama,'STATUS'=>$status);
		$dt   = array();
		
		if($nama != '' && $status!='') {
			$this->m_jabatan->getInsert($data);
			$dt = array('rs'=>1);
		} else {
			$dt = array('rs'=>0);
		}
		
		echo json_encode($dt);
	}
	
	function update() {
		$id = trim($_POST['id']);
		$nama = mysql_real_escape_string(trim($_POST['nama']));
		$status = mysql_real_escape_string(trim($_POST['status']));
		
		$data = array('JABATAN'=>$nama,'STATUS'=>$status);
		
		$this->db->update('tbl_jabatan',$data,array('ID'=>$id));
		
		$dt = array('rs'=>1);
		
		echo json_encode($dt);
	}
	
	function delete() {
		
		$id = trim($_POST['i']);
		$like = ('JABATAN = "'.$id.'"');
		$qry = $this->m_guru->count($like);
		
		if($qry>0) {
			$hsl = 0;
		} else {
			$this->db->where('ID',$id)
				->delete('tbl_jabatan');
			$hsl = 1;
		}
		
		
		$data = array(
			'rs' => $hsl
		);
		
		echo json_encode($data);
	}
	
}