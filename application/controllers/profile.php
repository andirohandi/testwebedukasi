<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			$this->load->helper('MY_date_helper');
			$this->load->helper(array('form'));
			$this->load->library('form_validation');
			$this->load->model('m_siswa');
			$this->load->model('m_guru');
			$this->load->helper('text');
	}
	
	function index() {
		$data['title'] 		= "Profile";
		$data['titlepage'] 	= "Profile ".$this->session->userdata('nama');
		$data['view'] 		= "profile/vwProfile";
		$this->load->view('vwIndex',$data);
	}
}