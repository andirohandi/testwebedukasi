<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			$this->load->helper('MY_date_helper');
			$this->load->helper(array('form'));
			$this->load->library('form_validation');
			$this->load->model('m_post');
			$this->load->model('m_front');
			$this->load->model('m_forum');
			$this->load->helper('text');
	}
	
	
	public function index()
	{
		$data['title'] 		= "SMA N 2 Bandung";
		$data['view'] 		= "content/vwContent";
		$data['pg'] 		= "home";
		$data['dash'] 		= "";
		$data['url_dash'] 	= "";
		$this->load->view('vwHome2',$data);
	}
	
	public function about()
	{
		$data['title'] 		= "SMA N 2 Bandung";
		$data['view'] 		= "content/vwAbout";
		$data['pg'] 		= "profile";
		$data['dash'] 		= "";
		$data['url_dash'] 	= "";
		$this->load->view('vwHome2',$data);
	}
	
	public function visimisi()
	{
		$data['title'] 		= "SMA N 2 Bandung";
		$data['view'] 		= "content/vwVisi";
		$data['pg'] 		= "profile";
		$data['dash'] 		= "";
		$data['url_dash'] 	= "";
		$this->load->view('vwHome2',$data);
	}
	
	function read($pg=1) {		
		$kategori	= '';
		$limit 		= 3;
		$key_text 	= '';
		$offset 	= ($limit*$pg)-$limit;
		
		$like = "((a.JUDUL like '%$key_text%' OR b.NAMA like '%$key_text%' OR b.NIP like '%$key_text%') AND a.KATEGORI != 3)";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_post->count($like,$kategori);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_pag($page);
		$where				= ("a.KATEGORI != 3");
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_post->getSelectHome($limit, $offset, $where);
		
		$this->load->view('content/vwList', $data);
	}
	
	function readKategori($pg=1,$kategori='') {		
		
		$limit 		= 9;
		$key_text 	= '';
		$offset 	= ($limit*$pg)-$limit;
		
		$like = "(a.JUDUL like '%$key_text%' OR b.NAMA like '%$key_text%' OR b.NIP like '%$key_text%')";
		
		$page = array();
		
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_post->count($like,$kategori);
		$page['current'] 	= $pg;
		$page['list'] 		= gen_pag($page);
		$where				= ("KATEGORI = $kategori");
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_post->getSelectHome($limit, $offset, $where);
		
		$this->load->view('content/vwListAll', $data);
	}
	
	function detail($pg='') {
		$p = str_replace(".html","",$pg);
		$data['title'] 		= $pg;
		$data['view'] 		= "content/vwDetail";
		$data['pg'] 		= "profile";
		$data['dash'] 		= "";
		$data['url_dash'] 	= "";
		$limit 		= 1;
		$offset		= '';
		$where = array(
			'URL' => $p
		);

		$data['list'] 		= $this->m_post->getSelectHome($limit, $offset, $where);
		$this->load->view('vwHome2',$data);
	}
	
	function kategori($kat='',$help='') {
		if($kat==1) {
			$judul = 'Informations';
			$pg = 'informasi';
		}else if($kat==2) {
			$judul = 'Articles';
			$pg = 'artikel';
		}else {
			$judul = 'Gallery';
			$pg = 'galeri';
		}
		
		$data['kat']		= $kat;
		$data['title'] 		= "SMA N 2 Bandung";
		$data['view'] 		= "content/vwKategori";
		$data['judul'] 		= $judul;
		$data['pg'] 		= $pg;
		$data['dash'] 		= "";
		$data['url_dash'] 	= "";
		$this->load->view('vwHome2',$data);
	}
	
	function forum() {
		
		$data['title'] 		= "SMA N 2 Bandung";
		$data['view'] 		= "content/vwForum";
		$data['pg'] 		= "forum";
		$data['dash'] 		= "";
		$data['url_dash'] 	= "";
		$this->load->view('vwHome2',$data);
	}
	
	function readForum($pg=1) {		
		
		$limit 		= 5;
		$key_text 	= '';
		$offset 	= ($limit*$pg)-$limit;
		$page = array();
		$page['limit'] 		= $limit;
		$page['count_row'] 	= $this->m_forum->count();
		$page['current'] 	= $pg;
		$page['list'] 		= gen_pag($page);
		$where				= '';
		$data['paging'] 	= $page;
		$data['list'] 		= $this->m_forum->getSelect( '', $limit, $offset);
		
		$this->load->view('content/vwListAllForum', $data);
	}
	
	function detailForum($ind='',$pg='') {
		$data['id'] = $ind;
		$p = str_replace(".html","",$pg);
		$data['title'] 		= $pg;
		$data['view'] 		= "content/vwDetailForum";
		$data['pg'] 		= "forum";
		$limit 		= 1;
		$offset		= '';
		$where = array(
			'URL' => $p
		);

		$data['list'] 		= $this->m_forum->getSelect($where, $limit, $offset);
		$this->load->view('vwHome2',$data);
	}
	
	function createComent() {
		$id_forum = $_POST['id_forum'];
		$komentar = $_POST['komentar'];
		
		$data = array (
		'KOMENTAR' => $komentar, 
		'TGL_INPUT' => date('Y-m-d'), 
		'NO_INDUK' => $this->session->userdata('username'), 
		'PENGIRIM' => $this->session->userdata('nama'),
		'ID_FORUM' => $id_forum,
		'LEVEL' => $this->session->userdata('level'),
		'STATUS' => 1
		);
		
		$query = $this->db->insert('tbl_komentar',$data);

		
		$msg = array('rs'=>1);
		
		echo json_encode($msg);
	}
	
	function deleteComent() {
		$id_komentar = $_POST['i'];
		
		$data = array(
			'ID' => $id_komentar,
		);
		$this->db->delete('tbl_komentar',$data);
		$msg = array('rs'=>1);
		
		echo json_encode($msg);
	}
}